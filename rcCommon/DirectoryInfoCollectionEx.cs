﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RcSoft.RcCommon
{
    public class DirectoryInfoCollectionEx : List<DirectoryInfoEx>
    {
        private int SortDate(DirectoryInfoEx m1, DirectoryInfoEx m2)
        {            
            return m1.DirInfo.LastWriteTime.CompareTo(m2.DirInfo.LastWriteTime);
        }

        private int SortName(DirectoryInfoEx m1, DirectoryInfoEx m2)
        {
            return m1.DirInfo.Name.CompareTo(m2.DirInfo.Name);
        }

        private int SortLength(DirectoryInfoEx m1, DirectoryInfoEx m2)
        {
            return m1.Length.CompareTo(m2.Length);
        }

        public void SortByDate()
        {
            this.Sort(SortDate);
        }

        public void SortByName()
        {
            this.Sort(SortName);
        }

        public void SortByLength()
        {
            this.Sort(SortLength);            
        }

        public void GetLength()
        {
            foreach (DirectoryInfoEx d in this)
            {
                d.GetLength();
            }
        }

        public bool Finished()
        {
            foreach (DirectoryInfoEx d in this)
            {
                if (!d.Processed)
                    return false;
            }
            return true;
        }
    }
}
