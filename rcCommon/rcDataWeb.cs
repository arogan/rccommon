/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using RcSoft.RcCommon;


namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for rcDataWeb.
	/// </summary>
	public class RcDataWeb : RcData
	{
		public RcDataWeb(string mdbFile) : base(mdbFile)
		{
			//
			// TODO: Add constructor logic here
			//
		}


		/// <summary>
		/// Save changes from a Datagrid back to it's datasource (datagrid)
		/// </summary>
		/// <param name="dg"></param>
		/// <param name="map"></param>
		public void SetChanges(DataGrid dg, string[,] map)
		{
			object key;
			DataRow dr;
			string col = "";
			string ctype = "";
			string cvalue = "";

			//loop through the rows and copy data from the datagrid to the dataset
			foreach (DataGridItem di in dg.Items)
			{
				//get the key value-primary key of the table
				key = dg.DataKeys[di.ItemIndex];
				//find the matching row in the data table
				dr = rcTable.Rows.Find(key);
				
				//update the row
				if (dr != null)
				{
					foreach (TableCell cell in di.Cells)
					{
						//loop through the controls
						foreach(Control ctl in cell.Controls)
						{
							string id = ctl.ID;
							if (id != null)
							{
								//search array to find dataset column name
								col = RcShared.SearchArray(id, map);
								if (col.Length > 0)
								{									
									//found the control with a corresponding column name map
									ctype = ctl.GetType().ToString();
									//dtype = dr[col].GetType().ToString();
								
									//get the value from the control in the datagrid
									switch (ctype)
									{
										case "System.Web.UI.WebControls.DropDownList":	
											DropDownList ddl = (DropDownList)ctl;
											cvalue = ddl.Items[ddl.SelectedIndex].Value.ToString();										
											break;
										case "System.Web.UI.WebControls.TextBox":
											cvalue = ((TextBox)(ctl)).Text;
											break;
										case "System.Web.UI.WebControls.CheckBox":											
											cvalue = ((CheckBox)(ctl)).Checked.ToString();
											break;
										default:
											throw new Exception("cannot handle control type: " + ctype);
											//break;
									}

									// if value has changed since retrieved from db then set the value back to the datarow
									if (cvalue != dr[col].ToString())
									{
//										if (dr[col].GetType() == System.DBNull)
//										{
//											dr[col] = System;
//										}
//										else
//										{
										
										dr[col] = Convert.ChangeType(cvalue,rcTable.Columns[col].DataType);	
										//}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
