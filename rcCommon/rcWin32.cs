/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class RcWin32
	{

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr SetFocus(IntPtr hWnd);
		private const UInt32 MOUSEEVENTF_LEFTDOWN = 0x0002;
		private const UInt32 MOUSEEVENTF_LEFTUP = 0x0004;

        [DllImport("user32.dll")]
        private static extern void mouse_event(
            UInt32 dwFlags, // motion and click options
            UInt32 dx, // horizontal position or change
            UInt32 dy, // vertical position or change
            UInt32 dwData, // wheel movement
            IntPtr dwExtraInfo // application-defined information
            );

		public const int RIGHTDOWN = 0x0008 ; /* right button down */
		public const int RIGHTUP = 0x0010 ; /* right button up */
		public const int MIDDLEDOWN = 0x0020 ; /* middle button down */
		public const int MIDDLEUP = 0x0040 ; /* middle button up */


        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        private const int SW_MINIMIZE = 6;
        private const int SW_MAXIMIZE = 3;
        private const int SW_RESTORE = 9;

        //public const int WM_SYSCOMMAND = 0x0112;
        //public const int SC_CLOSE = 0xF060;

        //[DllImport("user32.dll")]
        //public static extern int FindWindow(
        //    string lpClassName, // class name 
        //    string lpWindowName // window name 

        //);

        //[DllImport("user32.dll")]
        //public static extern int SendMessage(
        //    int hWnd, // handle to destination window 
        //    uint Msg, // message 
        //    int wParam, // first message parameter 
        //    int lParam // second message parameter 
        //); 

        //[DllImport("user32.dll")]
        //private static extern void keybd_event(byte bVk, byte bScan,
        //    int dwFlags, int dwExtraInfo);


		public RcWin32()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public static void SendClick(System.Drawing.Point location)
		{
			System.Windows.Forms.Cursor.Position = location;
			mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, new System.IntPtr());
			mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, new System.IntPtr());
		}

		public static void SendRClick(System.Drawing.Point location)
		{
			System.Windows.Forms.Cursor.Position = location;
			mouse_event(RIGHTDOWN, 0, 0, 0, new System.IntPtr());
			mouse_event(RIGHTUP, 0, 0, 0, new System.IntPtr());
		}

        public static Boolean setPFocus(Process p)
        {
            if (object.Equals(p, null))
                return false;
            IntPtr hWnd = p.MainWindowHandle;
            bool ret = SetForegroundWindow(hWnd);
            return ret;
        }

        public static void SetWindowState(string processName, ProcessWindowStyle pws)
        {
            foreach (Process thisproc in Process.GetProcessesByName(processName))
            {
                SetWindowState(thisproc,pws);
            }
        }

        public static void SetWindowState(Process p, ProcessWindowStyle pws)
        {
            IntPtr winHandle = p.MainWindowHandle;
            switch (pws)
            {
                case ProcessWindowStyle.Minimized:
                    ShowWindow(winHandle, SW_MINIMIZE);
                    break;
                case ProcessWindowStyle.Maximized:
                    ShowWindow(winHandle, SW_MAXIMIZE);
                    break;
            }
        }

        //public static string KillProcess(string processName)
        //{
        //    //System.Diagnostics.Process myproc= new System.Diagnostics.Process();
        //    //Get all instances of proc that are open, attempt to close them.


        //    foreach (Process thisproc in Process.GetProcessesByName(processName))
        //    {
        //        string pn = thisproc.ProcessName;

        //        if (!thisproc.CloseMainWindow())
        //        {
        //            //If closing is not successful or no desktop window handle, then force termination.		
        //            int i = thisproc.MainWindowHandle.ToInt32();
        //            int j = SendMessage(i, WM_SYSCOMMAND, SC_CLOSE, 0);
        //            if (thisproc.HasExited)
        //                return "close";

        //            thisproc.Kill();
        //            return "kill";
        //        }
        //        else
        //        {
        //            return "close";
        //        }
        //    } // next proc	
        //    return "";
        //}

        //public static void SendAltF4()
        //{
        //    keybd_event(0xa2, 0x1d, 0, 0); // Press Left CTRL
        //    keybd_event(0x7b, 0x58, 0, 0); // Press F12
        //    keybd_event(0x7b, 0xd8, 2, 0); // Release F12
        //    keybd_event(0xa2, 0x9d, 2, 0); // Release Left CTRL
        //}
	}
}
