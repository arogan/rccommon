/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
//using System.Web.Mail;
using System.Net.Mail;
//using DotNetOpenMail;
//using DotNetOpenMail.SmtpAuth;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for rcAlert.
	/// </summary>
	public class RcAlert
	{
		public string To;
		public string From;
		public string Server;

		public RcAlert()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        //new .net 2.0 way
        public static void SendEmail(string to, string from, string subject, string body, string server, string authName, string authPw)
        {
            //Create a new MailMessage object and specify the"From" and "To" addresses
            System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage(from, to);
            Message.Subject = subject;
            Message.Body = body;
            SmtpClient mailClient = new SmtpClient();
            mailClient.Host = server;            
            if (authName != "")
            {
                System.Net.NetworkCredential auth = new System.Net.NetworkCredential(authName,authPw);
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = auth;
                if (server.ToUpper().Contains("GMAIL"))
                {
                    mailClient.Port = 587;
                    mailClient.EnableSsl = true;
                }
            }
            else
            {
                mailClient.UseDefaultCredentials = true;
            }

            mailClient.Send(Message);
        }
        //old .net 1.1 way
        //public static void SendEmail(string to, string from, string subject, string body, string server, string authName, string authPw)
        //{
        //    if (object.Equals(to, null) || to.Length < 1)
        //        return;
        //    try
        //    {
        //        //System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage(from, to);
        //        MailMessage Message = new MailMessage();
        //        Message.To = to;
        //        Message.From = from;
        //        Message.Subject = subject;
        //        Message.Body = body;                              

        //        if (authName != "")
        //        {
        //            Message.Fields[@"http://schemas.microsoft.com/cdo/configuration/smtpserver"] = server;
        //            Message.Fields[@"http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = 25;
        //            Message.Fields[@"http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2;
        //            Message.Fields[@"http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
        //            Message.Fields[@"http://schemas.microsoft.com/cdo/configuration/sendusername"] = authName;
        //            Message.Fields[@"http://schemas.microsoft.com/cdo/configuration/sendpassword"] = authPw;
        //        }

        //        try
        //        {                    
        //            SmtpMail.SmtpServer = server;
        //            SmtpMail.Send(Message);
        //        }
        //        catch (System.Web.HttpException ehttp)
        //        {
        //            throw ehttp;
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //dotnetopenmail version
        //public static void SendEmail(string to, string from, string subject, string body, string server, string authName, string authPw)
        //{
        //    if (object.Equals(to, null) || to.Length < 1)
        //        return;
        //    try
        //    {
        //        EmailMessage emailMessage = new EmailMessage();
        //        emailMessage.FromAddress = new EmailAddress(from);
        //        string[] ary = to.Split(";".ToCharArray());
        //        foreach (string tostr in ary)
        //        {
        //            emailMessage.AddToAddress(new EmailAddress(tostr));
        //        }
        //        emailMessage.Subject = subject;
        //        emailMessage.TextPart = new TextAttachment(body);
        //        SmtpServer smtpServer=new SmtpServer(server);
        //        if (authName != string.Empty)
        //        {
        //            smtpServer.SmtpAuthToken = new SmtpAuthToken(authName, authPw);
        //        }
        //        emailMessage.Send(smtpServer);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static void SendEmail(string to, string from, string subject, string body, string server)
        {
            SendEmail(to, from, subject, body, server, "", "");
        }

		public void SendEmail(string subject, string body)
		{
			SendEmail(To,From,subject,body,Server);
		}
	}
}
