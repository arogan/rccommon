/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RcSoft.RcCommon;

namespace RcSoft.RcCommon
{
    public class rcGetWebObj
    {
        private WebBrowser wb = new WebBrowser();
        private bool isDone = false;
        private string ret = "";
        public int sec = 20;

        public rcGetWebObj() : this(20)
        {            
        }

        public rcGetWebObj(int asec)
        {
            sec = asec;
            wb = new WebBrowser();
            wb.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wb_DocumentCompleted);
            wb.ScriptErrorsSuppressed = true;
        }

        void wb_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            ret = wb.Document.Body.InnerHtml;

            //foreach (HtmlElement h in wb.Document.All)
            //{
            //    RcShared.WriteLog(h.InnerHtml);
            //}
            isDone = true;                
        }

        public string GetWeb(string url)
        {
            if (object.Equals(wb,null))
                throw new System.Exception("web object is null " + sec.ToString() + " seconds: " + url);            
            isDone = false;
            ret = "";
            wb.Stop();            
            wb.Navigate(url);

            for (int i = 0; i < sec * 4; i++)
            {                
                if (isDone)
                    return ret;
                RcShared.SleepMS(250);
                RcSharedEx.DoEvents();
            }
            throw new System.Exception("GetWeb timed out for " + sec.ToString() + " seconds: " + url);            
        }
    }
}
