﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Text;
using System.IO;
using System.Net;
using System.Web;
using System.Threading;
using System.ComponentModel;
using System.Net.Cache;

namespace RcSoft.RcCommon
{
    public class RcWebAsync        
    {
        public event DownloadStringCompletedEventHandler DownloadStringCompleted;
        public event DownloadProgressChangedEventHandler DownloadProgressChanged;
        public event AsyncCompletedEventHandler DownloadFileCompleted;

        private System.Timers.Timer timeout = new System.Timers.Timer(1000);
        private System.Timers.Timer stall = new System.Timers.Timer(1000);

        public int TimeoutSec = 14400;
        public int StallSec = 15;        
        public int Retry = 5;

        private int retryCount = 0;
        public int RetryCount
        {
            get { return retryCount; }
        }

        private int stallCount = 0;
        private int timeoutCount = 0;

        public WebClient request = new WebClient();
        public string Url;
        public bool DoCacheFix = false;

        public enum StatusEnum
        {
            Init,
            Downloading,
            Downloaded,
            Error,
            Aborted
        }

        public StatusEnum Status = StatusEnum.Init;


        public RcWebAsync()
        {
            request.DownloadStringCompleted += new DownloadStringCompletedEventHandler(request_DownloadStringCompleted);
            request.DownloadProgressChanged += new DownloadProgressChangedEventHandler(request_DownloadProgressChanged);
            request.DownloadFileCompleted +=new AsyncCompletedEventHandler(request_DownloadFileCompleted);

            timeout.Elapsed += new System.Timers.ElapsedEventHandler(timeout_Elapsed);
            stall.Elapsed += new System.Timers.ElapsedEventHandler(stall_Elapsed);

            request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);            

            retryCount = 0;                        
        }
   
        public void Reset()
        {
            StopTimers();
            timeoutCount = 0;
            stallCount = 0;
            retryCount = 0;
            Status = StatusEnum.Init;
        }

        public void ForceError()
        {
            Status = StatusEnum.Error;
            request.CancelAsync();
        }

        void stall_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            stallCount++;
            if (stallCount > StallSec)
            {
                StopTimers();
                RcShared.WriteError("(rcWebAsync)request stalled: " + Url);
                Status = StatusEnum.Error;
                request.CancelAsync();
            }
        }

        void timeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timeoutCount++;
            if (timeoutCount > TimeoutSec)
            {
                StopTimers();
                RcShared.WriteError("(rcWebAsync)request timed out: " + Url);
                Status = StatusEnum.Error;
                request.CancelAsync();
            }
        }

        public bool DownloadStringAsync(string address)
        {
            return DownloadStringAsync(address, null,"", "");
        }

        public bool DownloadStringAsync(string address, object userToken, string userid, string pw)
        {
            Url = address;
            
            //work around for this bug in webclient
            //http://silverlight.net/forums/p/14453/127178.aspx
            if (DoCacheFix)
            {
                string r = Guid.NewGuid().ToString();
                if (address.Contains("?"))
                    r = "&" + r;
                else
                    r = "?" + r;
                address = address + r;
            }

            if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(pw))
                request.Credentials = new NetworkCredential(userid, pw);
            
            if (!InitRequest())
                return false;
            if (userToken == null)
                request.DownloadStringAsync(new Uri(address));
            else
                request.DownloadStringAsync(new Uri(address), userToken);
            StartTimers();
            return true;
        }

        public bool DownloadFileAsync(string address, string fileName)
        {
            return DownloadFileAsync(address, fileName, null, "", "");
        }

        public bool DownloadFileAsync(string address, string fileName, object userToken, string userid, string pw )
        {
            Url = address;
            if (!InitRequest())
                return false;

            if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(pw))
                request.Credentials = new NetworkCredential(userid, pw);          

            if (userToken == null)          
                request.DownloadFileAsync(new Uri(address), fileName);
            else
                request.DownloadFileAsync(new Uri(address), fileName, userToken);
            StartTimers();
            return true;
        }

        private bool InitRequest()
        {            
            if (!CanRetry())
            {
                RcShared.WriteLog("(rcWebAsync)Tried request too many times: " + retryCount.ToString() + "; " + Url);
                return false;
            }
            retryCount++;
            Status = StatusEnum.Downloading;
            return true;
        }

        void request_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            StopTimers();
            if (Status == StatusEnum.Downloading)
                Status = StatusEnum.Downloaded;
            //bubble up the event
            if (this.DownloadStringCompleted != null)
                this.DownloadStringCompleted(sender, e);
        }

        void request_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            StopTimers();
            if (Status == StatusEnum.Downloading)
                Status = StatusEnum.Downloaded;
            //bubble up the event
            if (this.DownloadFileCompleted != null)
                this.DownloadFileCompleted(sender, e);
        }

        void request_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            stallCount = 0;
            //bubble up the event
            if (this.DownloadProgressChanged != null)
                this.DownloadProgressChanged(sender, e);            
        }

        private void StartTimers()
        {
            timeoutCount = 0;
            stallCount = 0;            
            if (TimeoutSec > 0)
                timeout.Start();
            if (StallSec > 0)
                stall.Start();
        }

        private void StopTimers()
        {
            timeout.Stop();
            stall.Stop();
        }

        public void Abort()
        {
            RcShared.WriteLog("(rcWebAsync)User Aborted request:" + Url);
            Status = StatusEnum.Aborted;
            request.CancelAsync();
        }

        public bool CanRetry()
        {
            if (Retry != 0 && retryCount + 1 > Retry)
                return false;
            else
                return true;
        }
    }
}