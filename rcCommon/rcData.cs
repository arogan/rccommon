/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

using System;
using System.Data;
using System.Data.OleDb;
using System.IO;

//using System.Reflection;

namespace RcSoft.RcCommon
{	
	/// <summary>
	/// Class to help use ado.net oledb with access files.  Only works with single table Datasets.
	/// This class if pretty fast but uses up quite a bit of memory since it maintances a reference
	/// to all ado objects used (dataset,connection,adapter,command,etc.)
	/// 
	/// You sould create one instance of this class for each sql statement you plan to work with.
	/// </summary>
	public class RcData
	{

		//global
		public OleDbCommandBuilder rcCB;

		#region Properties
		//Properties
		protected string connStr = "";		
		public string ConnStr
		{
			get
			{
				return connStr;
			}

			set
			{
				connStr = value;
			}
		}

		protected string sqlText = "";
		public string SqlText
		{
			get
			{
				return sqlText;
			}
			set
			{
				sqlText = value;
			}
		}

		protected string rcTableMap = "Table";
		public string RcTableMap
		{
			get
			{
				return rcTableMap;
			}
			set
			{
				rcTableMap = value;
			}
		}

		protected int primaryKeyCol = 0;
		public int PrimaryKeyCol
		{
			get
			{
				return primaryKeyCol;
			}
			set
			{
				primaryKeyCol = value;
			}
		}

		protected DataSet rcDataset = null;
		public DataSet RcDataSet
		{
			get
			{
				return rcDataset;
			}
			//			set
			//			{
			//				rcDataset = value;
			//			}
		}

		protected OleDbDataAdapter rcAdapter = null;
		public OleDbDataAdapter RcAdapter
		{
			get
			{
				return rcAdapter;
			}
			//			set
			//			{
			//				rcAdapter = value;
			//			}
		}

		protected DataTable rcTable = null;
		public DataTable RcTable
		{
			get
			{
				return rcTable;
			}
		}

        protected DataView rcView = null;
        public DataView RcView
        {
            get
            {
                return rcView;
            }
        }

		protected OleDbConnection rcConnection = null;
		public OleDbConnection RcConnection
		{
			get
			{
				return rcConnection;
			}
			//			set
			//			{
			//				rcConnection = value;
			//			}
		}

		protected OleDbCommand rcCommand = null;
		public OleDbCommand RcCommand
		{
			get
			{
				return rcCommand;
			}
			//			set
			//			{
			//				rcCommand = value;
			//			}
		}
		#endregion //Properties

		/// <summary>
		/// Build connection string using a user defined access mdb file.
		/// If Application.ExecutablePath is used then it will use a MDB file with the same name and path as the exe.
		/// also creates all required ado objects (dataset, command, connection, adapter, etc)
		/// </summary>
		/// <param name="mdbFile">Pass the mdb file including the full path or Application.ExecutablePath</param>
		public RcData(string mdbFile)
		{
			//			string theFile = "";
			//
			//			if (mdbFile == "") throw new Exception("Invalid string argument for connection string:" + mdbFile);
			//
			//			if (mdbFile.EndsWith(".mdb"))
			//			{
			//				theFile = mdbFile;				
			//			}
			//			else if (mdbFile.EndsWith(".exe"))
			//			{
			//				string s1, s2 = "";
			//				s1 = Path.GetDirectoryName(mdbFile);
			//				s2 = Path.GetFileNameWithoutExtension(mdbFile) + ".mdb";
			//				theFile = s1 + "\\" + s2;				
			//			}
			//			else
			//			{
			//				throw new Exception("Invalid string argument for connection string:" + mdbFile);
			//			}

			connStr = RcBuildConnStr(mdbFile);			

			//create objects. initalize properties.
			RcGetConnection();
			rcCommand = new OleDbCommand();
			rcAdapter = new OleDbDataAdapter();
			rcDataset = new DataSet();
			rcCommand.Connection = rcConnection;
			rcAdapter.SelectCommand = rcCommand;
			rcCB = new OleDbCommandBuilder(rcAdapter);
            rcCB.QuotePrefix = "[";
            rcCB.QuoteSuffix = "]";
		}

		/// <summary>
		/// Build connection string using a user defined access mdb file.
		/// If Application.ExecutablePath is used then it will use a MDB file with the same name and path as the exe.		
		/// </summary>
		/// <param name="mdbFile">Pass the mdb file including the full path or Application.ExecutablePath</param>
		/// <returns>Returns the connection string</returns>
		public static string RcBuildConnStr(string mdbFile)
		{
			string theFile = "";

			if (mdbFile == "") throw new Exception("Invalid string argument for connection string:" + mdbFile);

			if (mdbFile.ToLower().EndsWith(".mdb"))
			{
				theFile = mdbFile;				
			}
			else if (mdbFile.EndsWith(".exe"))
			{
				string s1, s2 = "";
				s1 = Path.GetDirectoryName(mdbFile);
				s2 = Path.GetFileNameWithoutExtension(mdbFile) + ".mdb";
				theFile = s1 + "\\" + s2;				
			}
			else
			{
				throw new Exception("Invalid string argument for connection string:" + mdbFile);
			}

			return @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + theFile + @";User Id=admin;Password=;";			

		}

		/// <summary>
		/// Gets a Connection object
		/// </summary>
		/// <returns>returns the conneciton object with the connection string set</returns>
		public OleDbConnection RcGetConnection() 
		{			
			OleDbConnection myConn = new OleDbConnection();
			myConn.ConnectionString = ConnStr;

			//update properties
			rcConnection = myConn;

			return myConn;
		}		
				
		/// <summary>
		/// Returns a dataset.  Call this to refresh a previously fetched dataset.
		/// It uses rcData properties to refresh the dataset.
		/// </summary>
		/// <returns>Dataset</returns>
		public DataSet RcGetDataset()
		{
			return RcGetDataset(sqlText);
		}

		/// <summary>
		/// Call this if you want a read only Dataset and you don't plan to do any updates.
		/// Note: there will be no schema information or table mapping.
		/// </summary>
		/// <param name="aSqlText">Sql select statement to execute.</param>
		/// <returns>dataset</returns>
		public DataSet RcGetDataset(string aSqlText)
		{
			try
			{							
				rcCommand.CommandText = aSqlText;				
				rcDataset.Clear();		
				rcAdapter.TableMappings.Clear();

				//adapter table mapping
				if (rcTableMap!="Table")
					rcAdapter.TableMappings.Add("Table", rcTableMap);		

				//fill the dataset
				rcConnection.Open();
				if (rcTableMap!="Table")
					rcAdapter.FillSchema(rcDataset,SchemaType.Source,rcTableMap);
				rcAdapter.Fill(rcDataset);
				rcConnection.Close();

				//update properties
				sqlText = aSqlText;	
				rcTable = rcDataset.Tables[0];
                rcView = rcTable.DefaultView;
		
				return rcDataset;
			}
			finally
			{
				if (rcConnection.State == ConnectionState.Open)
					rcConnection.Close();
			}
		}

		/// <summary>
		/// Get a fully updateable dataset with all schema information propery set.
		/// </summary>
		/// <param name="aSqlText">Sql select statement to execute.</param>
		/// <param name="tableName">Name of the table for mapping.</param>
		/// <returns>dataset</returns>
		public DataSet RcGetDataset(string aSqlText, string tableName)
		{
			try
			{							
				rcCommand.CommandText = aSqlText;				
				rcDataset.Clear();
				rcAdapter.TableMappings.Clear();

				//adapter table mapping
				rcAdapter.TableMappings.Add("Table", tableName);								

				//fill the dataset
				rcConnection.Open();
				rcAdapter.FillSchema(rcDataset,SchemaType.Source,tableName);
				rcAdapter.Fill(rcDataset);
				rcConnection.Close();

				//update properties
				sqlText = aSqlText;
				rcTableMap = tableName;		
				rcTable = rcDataset.Tables[0];
                rcView = rcTable.DefaultView;
				
				return rcDataset;
			}
			finally
			{
				if (rcConnection.State == ConnectionState.Open)
					rcConnection.Close();
			}
		}

		/// <summary>
		/// Updates a previoulsy fetched dataset.
		/// </summary>
		public void RcUpdate()
		{
			try
			{
				if (rcDataset!=null)
				{
					if (rcDataset.HasChanges())
					{                        
						rcCB.RefreshSchema();                                                
						rcAdapter.Update(rcDataset, rcTableMap);
                        rcDataset.AcceptChanges(); //rchen 2/26/09
					}
				}
			}
			finally
			{
				if (rcConnection.State == ConnectionState.Open)
					rcConnection.Close();
			}
		}

		/// <summary>
		/// Executes an update,insert, or delete
		/// Use "select count(*) ...." to return a count.
		/// </summary>
		/// <param name="aSqlText">Sql statement</param>
		/// <returns>Number of rows affected</returns>
		public int RcExecSql(string aSqlText)
		{
			
			OleDbCommand myCmd = new OleDbCommand();

			try
			{
				int ret;

				myCmd.Connection = rcConnection;
				myCmd.CommandText = aSqlText;				
				rcConnection.Open();
				if (aSqlText.ToUpper().StartsWith("SELECT"))
				{
					ret = (int) myCmd.ExecuteScalar();
				}
				else
				{
					ret = (int) myCmd.ExecuteNonQuery();				
				}
				rcConnection.Close();
				return ret;
			}
			finally
			{
				if (rcConnection.State == ConnectionState.Open)
					rcConnection.Close();
				myCmd = null;
			}

		}
	
		/// <summary>
		/// Static version.
		/// Executes an update,insert, or delete
		/// Use "select count(*) ...." to return a count.
		/// </summary>
		/// <param name="aSqlText">Sql statement</param>
		/// <param name="mdbFile">Pass the mdb file including the full path or Application.ExecutablePath</param>
		/// <returns>Number of rows affected</returns>
		public static int RcExecSql(string aSqlText, string mdbFile)
		{
			
			OleDbCommand myCmd = new OleDbCommand();
			OleDbConnection myConn = new OleDbConnection();
			int ret;

			try
			{
				myConn.ConnectionString = RcBuildConnStr(mdbFile);
				myCmd.Connection = myConn;
				myCmd.CommandText = aSqlText;
				myConn.Open();
				if (aSqlText.ToUpper().StartsWith("SELECT"))
				{
					ret = (int) myCmd.ExecuteScalar();
				}
				else
				{
					ret = myCmd.ExecuteNonQuery();						
				}
				myConn.Close();
				return ret;
			}
			finally
			{
				if (myConn.State == ConnectionState.Open)
					myConn.Close();
				myCmd = null;
				myConn = null;
			}
		}

		/// <summary>
		/// Get rid of those pesky nulls that blow up code when working with grids and other controls
		/// </summary>
		/// <param name="aRow">Datarow that contains the value you wish to check</param>
		/// <param name="aField">String name of the field</param>
		/// <param name="aDefault">Default value to return if field is null</param>
		/// <returns>value on specified row,column</returns>
		public static object RcIsNull(DataRow aRow, string aField, object aDefault)
		{
			if (object.Equals(aRow[aField],DBNull.Value))
			{
				return aDefault;
			}
			else
			{
				return aRow[aField];
			}
		}

		/// <summary>
		/// Get rid of those pesky nulls that blow up code when working with grids and other controls
		/// </summary>
		/// <param name="aRow">Datarow that contains the value you wish to check</param>
		/// <param name="aField">Integer of the field</param>
		/// <param name="aDefault">Default value to return if field is null</param>
		/// <returns>value on specified row,column</returns>
		public static object RcIsNull(DataRow aRow, int aField, object aDefault)
		{
			if (object.Equals(aRow[aField],DBNull.Value))
			{
				return aDefault;
			}
			else
			{
				return aRow[aField];
			}
		}
        public static object RcIsNull(object obj, object aDefault)
        {
            if (obj == null || object.Equals(obj, DBNull.Value))
            {
                return aDefault;
            }
            else
            {
                return obj;
            }
        }
	
		/// <summary>
		/// Use sqcommandbuilder to update all dirty records in the dataset
		/// </summary>
		public void RcUpdateBatch()
		{
			if (rcDataset.HasChanges())
			{
				OleDbCommandBuilder builder = new OleDbCommandBuilder(rcAdapter);
				rcAdapter.Update(rcTable);
			}
		}

        /// <summary>
        /// MBD compact method (c) 2004 Alexander Youmashev
        /// !!IMPORTANT!!
        /// !make sure there's no open connections
        ///    to your db before calling this method!
        /// !!IMPORTANT!!
        /// </summary>
        /// <param name="connectionString">connection string to your db</param>
        /// <param name="mdwfilename">FULL name
        ///     of an MDB file you want to compress.</param>
        public static void CompactAccessDB(string mdb)
        {            
            string connectionString = RcBuildConnStr(mdb);
            string mdwfilename = mdb;
            object[] oParams;

            //create an inctance of a Jet Replication Object
            object objJRO =
              Activator.CreateInstance(Type.GetTypeFromProgID("JRO.JetEngine"));

            //filling Parameters array
            //cnahge "Jet OLEDB:Engine Type=5" to an appropriate value
            // or leave it as is if you db is JET4X format (access 2000,2002)
            //(yes, jetengine5 is for JET4X, no misprint here)
            oParams = new object[] {
                connectionString,
                "Provider=Microsoft.Jet.OLEDB.4.0;Data" + 
                " Source=C:\\tempdb.mdb;Jet OLEDB:Engine Type=5"};

            //invoke a CompactDatabase method of a JRO object
            //pass Parameters array
            objJRO.GetType().InvokeMember("CompactDatabase",
                System.Reflection.BindingFlags.InvokeMethod,
                null,
                objJRO,
                oParams);

            //database is compacted now
            //to a new file C:\\tempdb.mdw
            //let's copy it over an old one and delete it

            System.IO.File.Delete(mdwfilename);
            System.IO.File.Move("C:\\tempdb.mdb", mdwfilename);

            //clean up (just in case)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(objJRO);
            objJRO = null;
        }
		
//		private void setDrValue(DataRow dr, string name, string avalue)
//		{
//			if (dr != null)
//			{
//				string dtype = dr[name].GetType().ToString();
//
//				switch (dtype)
//				{
//					case "System.Int32":
//						dr[name] = Convert.ToInt32(avalue);
//						break;
//					case "System.Double":
//						break;
//					case "System.String":
//						break;
//					case "System.Boolean":
//						break;
//					case "System.DateTime":
//						break;
//					default:
//						throw new Exception("invalid datarow column type: " + dtype);
//				}
//			}
//		}

        public static DataTable ImportXLS(string aFile)
        {
            return ImportXLS(aFile, "Sheet1","");
        }
		
        public static DataTable ImportXLS(string aFile, string aSheet, string sortOrder)
        {
           DataTable dt = new DataTable();
            OleDbConnection dbConnection =  new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;"
                    + @"Data Source=" + aFile + ";" + @"Extended Properties=""Excel 8.0;HDR=Yes;""");
            dbConnection.Open();
            try
            {
                string so = "";
                if (sortOrder != String.Empty)
                    so = " order by " + sortOrder;
                OleDbDataAdapter dbAdapter =
                    new OleDbDataAdapter
                        ("SELECT * FROM [" + aSheet + "$]" + so, dbConnection);
                dbAdapter.Fill(dt);
            }
            finally
            {
                dbConnection.Close();
            }
            return dt;
        }
		
	} //end clase
} //end namespace
