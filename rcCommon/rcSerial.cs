/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Xml;
using System.Xml.Serialization;
using RcSoft.RcCommon;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for rcSerial.
	/// </summary>
	public class RcSerial
	{
		public RcSerial()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public static void SaveToFile(object aObj, string aFullFile)
		{
			try
			{
				XmlSerializer ser = new XmlSerializer(aObj.GetType());			
				for (int i = 1; i <= 4; i++)
				{
					try
					{

						using (FileStream stream = File.Open(aFullFile,FileMode.Create,FileAccess.Write,FileShare.None))
						{
							ser.Serialize(stream,aObj);
							stream.Flush();
							stream.Close();
							return;
						}
					}
					catch (Exception ex)
					{
						RcShared.WriteLog("saveToFile: " + aFullFile + ": failed while trying to save on attempt: " + 
							i.ToString() + "; " + ex.Message) ;
						RcShared.Sleep(5);
					}
				}
				throw new Exception("SaveToFile failed.  tried too many times: " + aFullFile);
			}
			catch (System.Exception ex)
			{
				RcShared.WriteLog(ex.Message);
				throw ex;
			}	
		}

		/// <summary>
		/// deserialize object
		/// </summary>
		/// <param name="derivedType">use yourobject.GetType()</param>
		/// <param name="aFullFile"></param>
		/// <returns>don't forget to cast it back to your specific objec type</returns>
		public static object LoadFromFile(System.Type derivedType, string aFullFile)
		{
			//System.Type derivedType = aObj.GetType();
			object ret = null;
			try
			{				
				RcShared.ElapsedTime et = new RcShared.ElapsedTime();

				for (int x=1;x <= 5;x++)
				{
					if (!File.Exists(aFullFile))
						RcShared.Sleep(2);
					else
						break;
				}

				if (File.Exists(aFullFile))
				{
					int i = 1;
					for (i = 1; i <= 4; i++)
					{
						try
						{							
							XmlSerializer ser = new XmlSerializer(derivedType);			
							using (FileStream stream = File.OpenRead(aFullFile))
							{
								ret = ser.Deserialize(stream);			
								break;
							}
						}
						catch (Exception ex)
						{
							RcShared.WriteLog("LoadFromFile: " + aFullFile + ": failed while trying to load on attempt: " + 
								i.ToString() + "; " + ex.Message) ;
							RcShared.Sleep(5);
						}
					}
					if (i > 4)					
						throw new Exception("LoadFromFile failed. tried too many times: " + aFullFile);
				}
				else
				{
					throw new Exception("file does not exist: " + aFullFile);
				}
				return ret;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// deserialize object
		/// </summary>
		/// <param name="derivedType">use yourobject.GetType()</param>
		/// <param name="aFullFile"></param>
		/// <returns>don't forget to cast it back to your specific objec type</returns>
		public static object LoadFromFileFast(System.Type derivedType, string aFullFile)
		{
			//System.Type derivedType = aObj.GetType();
			object ret = null;
			try
			{				
				RcShared.ElapsedTime et = new RcShared.ElapsedTime();

				if (File.Exists(aFullFile))
				{
					try
					{							
						XmlSerializer ser = new XmlSerializer(derivedType);			
						using (FileStream stream = File.OpenRead(aFullFile))
						{
							ret = ser.Deserialize(stream);			
						}
					}
					catch (Exception ex)
					{
						RcShared.WriteLog("LoadFromFileFast: " + aFullFile + ": failed while trying to load; " + ex.Message) ;
						throw new Exception("LoadFromFileFast failed. tried too many times: " + aFullFile);
					}					
				}
				else
				{
					throw new Exception("file does not exist: " + aFullFile);
				}
				return ret;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		public static void DeleteXml(string aFullFile)
		{
			for (int i = 1; i <= 4; i++)
			{
				try
				{
					if (File.Exists(aFullFile))				
					{
						File.Delete(aFullFile);
						return;
					}
					else
						return;
				}
				catch (Exception ex)
				{
					RcShared.WriteLog("DeleteXml: " + aFullFile + ": failed while trying to delete on attempt: " + 
						i.ToString() + "; " + ex.Message) ;
					RcShared.Sleep(5);
				}
			}
			throw new System.Exception("DeleteXml failed.  tried too many times: " + aFullFile);
		}

        public static string SerializeBase64(object aObj)
        {
            
            BinaryFormatter ser = new BinaryFormatter();
            byte[] bytes;
            long length = 0;
            
            using (MemoryStream stream = new MemoryStream())
            {                
                ser.Serialize(stream, aObj);
                stream.Flush();
                length = stream.Length;                
                stream.Position = 0;
                bytes = stream.GetBuffer();
                string encodedData = bytes.Length + ":" + Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None);
                return encodedData;                
            }
        }

        public static object DeserializeBase64(string s)
        {
            object o = null;
            try
            {
                // We need to know the exact length of the string - Base64 can sometimes pad us by a byte or two
                int p = s.IndexOf(':');
                int length = Convert.ToInt32(s.Substring(0, p));

                // Extract data from the base 64 string!
                byte[] memorydata = Convert.FromBase64String(s.Substring(p + 1));
                MemoryStream rs = new MemoryStream(memorydata, 0, length);
                BinaryFormatter sf = new BinaryFormatter();
                o = sf.Deserialize(rs);
                return o;
            }
            catch (System.Exception ex)
            {
                RcShared.WriteEx(ex);
                return null;
            }
        }

	} //end class
} //end namespace
