using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

namespace RcSoft.RcCommon
{
	/// <summary>
	///		A Collection of FileInfo objects, together with advanced options for finding and sorting files.
    ///		http://www.asp101.com/articles/christopher/filesystem/default.asp
	/// </summary>
	public class FileInfoCollection : CollectionBase
	{
		#region Constructors
		
		/// <summary>Creates a new empty FileInfoCollection.</summary>
		public FileInfoCollection() { /*do nothing */ }

		/// <summary>Creates a new FileInfoCollection populated with data.</summary>
		/// <param name="Items">An array of FileInfo with which to populate.</param>
		public FileInfoCollection(FileInfo[] Items)
		{
			this.AddRange(Items);
		}

		#endregion

		#region Public Properties
		
		/// <summary>Gets or Sets the FileInfo object at the given Index.</summary>
		public FileInfo this[int Index]
		{
			get { return (FileInfo)this.InnerList[Index]; }
			set { this.InnerList[Index] = value; }
		}

		#endregion

		#region Add/Remove Functions

		/// <summary>Adds a new FileInfo object into the end of the collection.</summary>
		/// <param name="Item">The FileInfo object to add.</param>
		public void Add(FileInfo Item)
		{
			this.InnerList.Add(Item);
		}

		/// <summary>Adds a collection of FileInfo objects into the end of the collection.</summary>
		/// <param name="Items">The array of FileInfo objects to add.</param>
		public void AddRange(FileInfo[] Items)
		{
			this.InnerList.AddRange(Items);
		}

		/// <summary>Adds a collection of FileInfo objects into the end of the collection.</summary>
		/// <param name="Items">The FileInfoCollection containing the FileInfo objects to add.</param>
		public void AddRange(FileInfoCollection Items)
		{
			this.InnerList.AddRange(Items);
		}

		/// <summary>Removes the specified FileInfo from the collection.</summary>
		/// <param name="Item">The FileInfo object to remove.</param>
		public void Remove(FileInfo Item)
		{
			this.InnerList.Remove(Item);
		}

		#endregion

		#region Sorting Functions

		/// <summary>Sorts the FileInfo objects in the collection using the specified comparer.</summary>
		/// <param name="Comparer">The comparer to use to determine the sorting of the objects.</param>
		public void Sort(IComparer Comparer)
		{
			this.InnerList.Sort(Comparer);
		}

		/// <summary>Sorts the FileInfo objects in the collection by Name.</summary>
		public void SortByName()
		{
			this.InnerList.Sort(new FileInfoNameComparer());
		}

		/// <summary>Sorts the FileInfo objects in the collection by Extension.</summary>
		public void SortByExtension()
		{
			this.InnerList.Sort(new FileInfoExtensionComparer());
		}

		/// <summary>Sorts the FileInfo objects in the collection by LastWriteTime.</summary>
		public void SortByLastWriteTime()
		{
			this.InnerList.Sort(new FileInfoLastWriteTimeComparer());
		}

		/// <summary>Sorts the FileInfo objects in the collection by Length.</summary>
		public void SortByLength()
		{
			this.InnerList.Sort(new FileInfoLengthComparer());
		}

		/// <summary>Reverses the order of the FileInfo objects in the collection.</summary>
		public void Reverse()
		{
			this.InnerList.Reverse();
		}

		#endregion

		#region Searching Functions

		/// <summary>Does the specified file exist within the collection?</summary>
		/// <param name="FilePath">The file to look for.</param>
		/// <returns>Whether the file exists within the collection.</returns>
		public bool Contains(string FilePath)
		{
			foreach (FileInfo objFile in this)
			{
				if (objFile.FullName == FilePath)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Does the specified file exist within the collection?</summary>
		/// <param name="File">The file to look for.</param>
		/// <returns>Whether the file exists within the collection.</returns>
		public bool Contains(FileInfo File)
		{
			return this.InnerList.Contains(File);
		}

		/// <summary>Searches within the collection for the specified FileInfo object.</summary>
		/// <param name="File">The File to search for.</param>
		/// <returns>The zero-based index of the FileInfo object within the collection.</returns>
		public int IndexOf(FileInfo File)
		{
			return this.InnerList.IndexOf(File);
		}

		/// <summary>
		///		Searches within the collection for any FileInfo objects who's name
		///		matches the supplied expression.
		/// </summary>
		/// <param name="Match">The Regular Expression to search with.</param>
		/// <returns>A collection of matching FileInfo objects.</returns>
		public FileInfoCollection FilterByName(Regex Match)
		{
			FileInfoCollection objReturn = new FileInfoCollection();
			foreach (FileInfo objItem in this)
			{
				if (Match.IsMatch(objItem.Name))
				{
					objReturn.Add(objItem);
				}
			}
			return objReturn;
		}

		#endregion

		#region Comparer Internal Classes

		internal class FileInfoNameComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				FileInfo objX = (FileInfo)x;
				FileInfo objY = (FileInfo)y;
				return objX.Name.CompareTo(objY.Name);
			}
		}

		internal class FileInfoExtensionComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				FileInfo objX = (FileInfo)x;
				FileInfo objY = (FileInfo)y;
				return objX.Extension.CompareTo(objY.Extension);
			}
		}
		
		internal class FileInfoLastWriteTimeComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				FileInfo objX = (FileInfo)x;
				FileInfo objY = (FileInfo)y;
				return objX.LastWriteTime.CompareTo(objY.LastWriteTime);
			}
		}

		internal class FileInfoLengthComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				FileInfo objX = (FileInfo)x;
				FileInfo objY = (FileInfo)y;
				return objX.Length.CompareTo(objY.Length);
			}
		}

		#endregion
	}
}