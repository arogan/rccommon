using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace RcSoft.RcCommon
{
    public class NativeWin32
    {
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_CLOSE = 0xF060;

        [DllImport("user32.dll")]
        public static extern int FindWindow(
            string lpClassName, // class name 
            string lpWindowName // window name 
        );

        [DllImport("user32.dll")]
        public static extern int SendMessage(
            int hWnd, // handle to destination window 
            uint Msg, // message 
            int wParam, // first message parameter 
            int lParam // second message parameter 
        );

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(
            int hWnd // handle to window
            );

        private const int GWL_EXSTYLE = (-20);
        private const int WS_EX_TOOLWINDOW = 0x80;
        private const int WS_EX_APPWINDOW = 0x40000;
              
        public const int GW_HWNDFIRST = 0;
        public const int GW_HWNDLAST  = 1;
        public const int GW_HWNDNEXT  = 2;
        public const int GW_HWNDPREV  = 3;
        public const int GW_OWNER     = 4;
        public const int GW_CHILD     = 5;

        public delegate int EnumWindowsProcDelegate(int hWnd, int lParam);

        [DllImport("user32")]
        public static extern int EnumWindows(EnumWindowsProcDelegate lpEnumFunc, int lParam);

        [DllImport("User32.Dll")]
        public static extern void GetWindowText(int h, StringBuilder s, int nMaxCount);

        [DllImport("user32", EntryPoint = "GetWindowLongA")]
        public static extern int GetWindowLongPtr(int hwnd, int nIndex);

        [DllImport("user32")]
        public static extern int GetParent(int hwnd);

        [DllImport("user32")]
        public static extern int GetWindow(int hwnd, int wCmd);

        [DllImport("user32")]
        public static extern int IsWindowVisible(int hwnd);

        [DllImport("user32")]
        public static extern int GetDesktopWindow();
    }

      //Adding all SendKeys class supported keys in the 'All Keys' listbox
            //lbAllKeys.Items.Add("{BS}");
            //lbAllKeys.Items.Add("{BREAK}");
            //lbAllKeys.Items.Add("{CAPSLOCK}");
            //lbAllKeys.Items.Add("{DEL}");
            //lbAllKeys.Items.Add("{DOWN}");
            //lbAllKeys.Items.Add("{END}");
            //lbAllKeys.Items.Add("{ENTER}");
            //lbAllKeys.Items.Add("{ESC}");
            //lbAllKeys.Items.Add("{HELP}");
            //lbAllKeys.Items.Add("{HOME}");
            //lbAllKeys.Items.Add("{INSERT}");
            //lbAllKeys.Items.Add("{LEFT}");
            //lbAllKeys.Items.Add("{NUMLOCK}");
            //lbAllKeys.Items.Add("PGDN}");
            //lbAllKeys.Items.Add("{PGUP}");
            //lbAllKeys.Items.Add("{PRTSC}");
            //lbAllKeys.Items.Add("{RIGHT}");
            //lbAllKeys.Items.Add("{SCROLLLOCK}");
            //lbAllKeys.Items.Add("{SPACE}");
            //lbAllKeys.Items.Add("{TAB}");
            //lbAllKeys.Items.Add("{UP}");
            //lbAllKeys.Items.Add("{F1}");
            //lbAllKeys.Items.Add("{F2}");
            //lbAllKeys.Items.Add("{F3}");
            //lbAllKeys.Items.Add("{F4}");
            //lbAllKeys.Items.Add("{F5}");
            //lbAllKeys.Items.Add("{F6}");
            //lbAllKeys.Items.Add("{F7}");
            //lbAllKeys.Items.Add("{F8}");
            //lbAllKeys.Items.Add("{F9}");
            //lbAllKeys.Items.Add("{F10}");
            //lbAllKeys.Items.Add("{F11}");
            //lbAllKeys.Items.Add("{F12}");
            //lbAllKeys.Items.Add("{F13}");
            //lbAllKeys.Items.Add("{F14}");
            //lbAllKeys.Items.Add("{F15}");
            //lbAllKeys.Items.Add("{F16}");
            //lbAllKeys.Items.Add("{ADD}");
            //lbAllKeys.Items.Add("{SUBTRACT}");
            //lbAllKeys.Items.Add("{MULTIPLY}");
            //lbAllKeys.Items.Add("{DIVIDE}");

            //lbAllKeys.Items.Add("SHIFT (+)");
            //lbAllKeys.Items.Add("CTRL (^)");
            //lbAllKeys.Items.Add("ALT (%)");

    public class rcSendKeys
    {
        private List<string> winTitles = null;
        private string defaultWindowTitle = "";
        private int defaultHandle = 0;

        public string DefaultWindowTitle
        {
            get { return defaultWindowTitle; }
            set
            {
                setDefaultWindowTitle(value);
            }
        }

        public rcSendKeys()
        {
            GetTaskWindows();
        }

        /// <summary>
        /// partial strings are supported
        /// </summary>
        /// <param name="windowTitle"></param>
        private bool setDefaultWindowTitle(string windowTitle)
        {
            foreach (string s in winTitles)
            {
                if (s.ToUpper().Contains(windowTitle.ToUpper()))
                {
                    defaultWindowTitle = s;
                    defaultHandle = NativeWin32.FindWindow(null, defaultWindowTitle); ;
                    return true;
                }
            }
            return false;
        }

        public void SendKeys(string keys)
        {
            //int iHandle = NativeWin32.FindWindow(null, defaultWindowTitle);
            //if (defaultHandle == 0)
            //    defaultHandle = NativeWin32.FindWindow(null, defaultWindowTitle);
            RcShared.WriteLog("sendkeys handle = " + defaultHandle.ToString());
            NativeWin32.SetForegroundWindow(defaultHandle);
            System.Windows.Forms.SendKeys.Send(keys);
            //RcShared.Sleep(3);
        }

        public void SendKeys(string keys, string windowTitle)
        {
            string finalWindow = windowTitle;
            foreach (string s in winTitles)
            {
                if (s.ToUpper().Contains(windowTitle.ToUpper()))
                {
                    finalWindow = s;
                    break;
                }
            }
            int iHandle = NativeWin32.FindWindow(null, finalWindow);
            NativeWin32.SetForegroundWindow(iHandle);            
            System.Windows.Forms.SendKeys.Send(keys);
        }

        public void RefreshWindowTitles()
        {
            GetTaskWindows();
        }

        public void SetForeground(string windowTitle)
        {
            int iHandle = NativeWin32.FindWindow(null, windowTitle);
            NativeWin32.SetForegroundWindow(iHandle);    
        }

        private void GetTaskWindows()
        {
            winTitles = new List<string>();

            // Get the desktopwindow handle
            int nDeshWndHandle = NativeWin32.GetDesktopWindow();
            // Get the first child window
            int nChildHandle = NativeWin32.GetWindow(nDeshWndHandle, NativeWin32.GW_CHILD);

            while (nChildHandle != 0)
            {
                //If the child window is this (SendKeys) application then ignore it.
                //if (nChildHandle == this.Handle.ToInt32())
                //{
                //    nChildHandle = NativeWin32.GetWindow(nChildHandle, NativeWin32.GW_HWNDNEXT);
                //}

                // Get only visible windows
                if (NativeWin32.IsWindowVisible(nChildHandle) != 0)
                {
                    StringBuilder sbTitle = new StringBuilder(1024);
                    // Read the Title bar text on the windows to put in combobox
                    NativeWin32.GetWindowText(nChildHandle, sbTitle, sbTitle.Capacity);
                    String sWinTitle = sbTitle.ToString();
                    {
                        if (sWinTitle.Length > 0)
                        {
                            winTitles.Add(sWinTitle);                            
                        }
                    }
                }
                // Look for the next child.
                nChildHandle = NativeWin32.GetWindow(nChildHandle, NativeWin32.GW_HWNDNEXT);
            }
        }
    }
}
