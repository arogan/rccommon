/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using RcSoft.RcCommon;

namespace RcSoft.RcCommon
{
    public class RcProcess
    {
        /// <summary>
        /// add the following to your class:
        ///	private System.Diagnostics.Process process1 = new System.Diagnostics.Process();
        ///	
        ///	this.process1.SynchronizingObject = this;
        /// this.process1.Exited += new System.EventHandler(this.process1_Exited);
        /// 
        /// private void process1_Exited(object sender, System.EventArgs e)
        /// 
        /// example call:
        /// RcShared.RunAsync(process1,"notepad","");
        /// </summary>
        /// <param name="p"></param>
        /// <param name="cmd"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public void RunAsync(Process p, string cmd, string arg, ProcessWindowStyle winStyle)
        {
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.EnableRaisingEvents = true;
                p.StartInfo.UseShellExecute = true;               
                p.StartInfo.WindowStyle = winStyle;
                p.Start();
            }
            catch (System.Exception ex)
            {
                RcShared.WriteEx("RunShellAsync", ex);
                throw ex;
            }
        }

        public static void RunAsyncNoRet(string cmd, string arg, ProcessWindowStyle winStyle)
        {
            Process p = new Process();

            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(cmd);
                p.EnableRaisingEvents = false;
                p.StartInfo.UseShellExecute = true;
                p.StartInfo.RedirectStandardOutput = false;
                p.StartInfo.RedirectStandardError = false;           
                p.StartInfo.WindowStyle = winStyle;
                p.Start();
                
            }
            catch (System.Exception ex)
            {
                RcShared.WriteEx("RunAsyncNoRet", ex);
                throw ex;
            }
        }

        public static int RunSyncHide(string cmd, string arg)
        {
              Process p = new Process();
              try
              {
                  int ret = -999;

                  p.StartInfo.FileName = cmd;
                  p.StartInfo.UseShellExecute = false;
                  p.StartInfo.WorkingDirectory = Path.GetDirectoryName(cmd);
                  p.StartInfo.Arguments = arg;
                  p.StartInfo.RedirectStandardOutput = false;
                  p.StartInfo.RedirectStandardError = false;
                  p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                  p.StartInfo.CreateNoWindow = true;                  

                  p.Start();
                  p.WaitForExit();
                  if (p.HasExited)
                  {
                      ret = p.ExitCode;
                  }
                  p.Close();
                  return ret;
              }
              catch (System.Exception ex)
              {
                  RcShared.WriteEx("RunSyncHide", ex);
                  throw ex;
              }
        }    
    } //end class
} //end namspace
