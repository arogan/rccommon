/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Xml;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for rcXML.
	/// </summary>
	public class RcXML
	{
		public RcXML()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        public static bool NodeExists(XmlNode n, string xpath)
        {
            XmlNode x = n.SelectSingleNode(xpath);
            if (Object.Equals(x, null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

		public static string GetNodeValue(XmlNode n, string xpath)
		{
			XmlNode x = n.SelectSingleNode(xpath);
			if (Object.Equals(x,null))
			{
				return "";
			}
			else
			{
				return x.InnerText;
			}
		}

		public static int GetNodeValueInt(XmlNode n, string xpath)
		{
			XmlNode x = n.SelectSingleNode(xpath);
			if (Object.Equals(x,null))
			{
				return 0;
			}
			else
			{
				return int.Parse(x.InnerText);
			}
		}

		public static double GetNodeValueDbl(XmlNode n, string xpath)
		{
			XmlNode x = n.SelectSingleNode(xpath);
			if (Object.Equals(x,null))
			{
				return 0.0;
			}
			else
			{
				return double.Parse(x.InnerText);
			}
		}

		public static string[] NodeToArray(XmlNode n, string xpath)
		{
			XmlNodeList nl = n.SelectNodes(xpath);
			string[] ary = new string[nl.Count];
			int index = 0;
			foreach(XmlNode ni in nl)				
			{
				ary[index] = ni.InnerText;
				index++;
			}

			return ary;
		}
	}
}
