/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Web;
using System.Net;
using System.IO;
using System.Data;
using System.Text;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for rcSharedWeb.
	/// </summary>
	public class RcSharedWeb : RcShared
	{
		public RcSharedWeb()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static string GetUrl(string aUrl)
		{
            return GetUrl(aUrl, 0);
		}

        public static string GetUrl(string aUrl, int timeout)
        {
            string s = "";
            HttpWebRequest req = (HttpWebRequest)System.Net.HttpWebRequest.Create(aUrl);
            req.Method = "GET";
            req.KeepAlive = false;
            req.ContentType = "text/html";
            if (timeout != 0)
                req.Timeout = timeout;
            HttpWebResponse rsp = null;
            rsp = (HttpWebResponse)req.GetResponse();
            if (rsp.StatusCode != HttpStatusCode.OK)
                throw new System.Exception("error: " + rsp.StatusCode.ToString());
            s = RspToString(rsp);
            rsp.Close();
            return s;
        }

        public static string GetUrlResponse(string aUrl, ref HttpWebResponse rsp)
        {
            string s = "";
            HttpWebRequest req = (HttpWebRequest)System.Net.HttpWebRequest.Create(aUrl);
            req.Method = "GET";
            req.KeepAlive = false;
            req.ContentType = "text/html";
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (System.Exception ex)
            {
                throw ex;                
            }
            if (rsp.StatusCode != HttpStatusCode.OK)
            {
                rsp.Close();
                throw new System.Exception("error: " + rsp.StatusCode.ToString());
            }
            s = RspToString(rsp);
            rsp.Close();
            return s;
        }


		public static string RspToString(HttpWebResponse r)
		{
			
			Stream rStream = r.GetResponseStream();		
			StreamReader sr = new StreamReader(rStream);	
			string ret = sr.ReadToEnd();
			sr.Close();		
			rStream.Close();
		
			return ret;
		}

		public static void ForceSSL(System.Web.HttpRequest r, System.Web.HttpResponse rsp)
		{
			if (!RcShared.InIDE())
			{
				if (Convert.ToInt32(r.ServerVariables["SERVER_PORT"]) == 80)
				{
					string url = @"https://" + (string)r.ServerVariables["SERVER_NAME"] + (string)r.ServerVariables["URL"];
					rsp.Redirect(url, true);
				}
			}
		}

        /// <summary>
        /// formdata should be name=value&name2=value2&name3=value3
        /// </summary>
        /// <param name="aURL"></param>
        /// <param name="formData"></param>
        /// <returns></returns>
        public static string PostURL(string aURL, string formData)
        {
            HttpWebRequest req = (HttpWebRequest)System.Net.HttpWebRequest.Create(aURL);
            req.Method = "POST";
            req.KeepAlive = false;
            req.ContentType = "application/x-www-form-urlencoded";
            
            //build form to post
            ASCIIEncoding encoding = new ASCIIEncoding();
            string postData = formData;

            //encode data
            byte[] data = encoding.GetBytes(postData);
            req.ContentLength = data.Length;
            Stream newStream = req.GetRequestStream();
            // Send the data.
            newStream.Write(data, 0, data.Length);
            newStream.Close();
           
            HttpWebResponse rsp = (HttpWebResponse)req.GetResponse();
            if (rsp.StatusCode != HttpStatusCode.OK)
                throw new System.Exception("error: " + rsp.StatusCode.ToString());
            string s = RspToString(rsp);
            rsp.Close();

            return s;
            
        }
	}//end class
}//end namespace
