/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;


namespace RcSoft.RcCommon
{
	/// <summary>
	/// page level session management
	/// kind of a wrapper for session to keep name collisions from happening on different pages in the
	/// same applicaton.  Just use a unique prefix for every page.
	/// </summary>	

	public class RcSession : System.Web.HttpApplication
	{

		protected System.Web.HttpContext hc;
		protected string pre;

		protected ArrayList keys;
		public ArrayList Keys
		{
			get
			{
				keys = new ArrayList();
				foreach (string k in hc.Session.Keys)
				{
					if (k.StartsWith(pre))
					{
						keys.Add(k);
					}
				}
				return keys;
			}
		}


		public RcSession(System.Web.HttpContext context, string prefix)
		{
			hc = context;
			pre = prefix;
		}
		public void Remove(string name)
		{			
			hc.Session[pre + name] = "";			
			hc.Session.Remove(pre + name);						
		}

		public int RemoveAll()
		{		
			int	i = 0;
			foreach(string k in this.Keys)
			{
				if (k.StartsWith(pre))
				{
					Remove(k);
					i++;
				}
			}
			return i;
		}
		public void Add(string name, object value)
		{
			hc.Session.Add(pre + name, value);						
		}

		public void Items(string name, object value)
		{
			hc.Session.Add(pre + name, value);				
		}

		public object Items(string name)
		{
			return hc.Session[pre + name];
		}

		public bool Exists(string name)
		{
			if (object.Equals(hc.Session[pre + name],null))
				return false;
			else
				return true;
		}
		
	}
}
