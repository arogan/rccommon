using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

namespace RcSoft.RcCommon
{
	/// <summary>
	///		A Collection of DirectoryInfo objects, together with
	///		advanced options for finding and sorting Directories.
    ///		http://www.asp101.com/articles/christopher/filesystem/default.asp
	/// </summary>
	public class DirectoryInfoCollection : CollectionBase
	{
		#region Constructors

		/// <summary>Creates a new empty DirectoryInfoCollection.</summary>
		public DirectoryInfoCollection() { /* Do Nothing */ }

		/// <summary>Creates a new DirectoryInfoCollection populated with data.</summary>
		/// <param name="Items">An array of DirectoryInfo objects with which to populate.</param>
		public DirectoryInfoCollection(DirectoryInfo[] Items)
		{
			this.AddRange(Items);
		}

        public DirectoryInfoCollection(string startPath)
        {
            fillDirs(startPath);
            this.Add(new DirectoryInfo(startPath));
            this.SortByName();
        }

        private void fillDirs(string startPath)
        {
            if (Directory.Exists(startPath))
            {
                DirectoryInfo di = new DirectoryInfo(startPath);
                foreach (DirectoryInfo d in di.GetDirectories())
                {
                    //recurse                    
                    fillDirs(d.FullName);
                    this.Add(d);
                }
            }
        }
		#endregion

		#region Public Properties

		/// <summary>Gets or Sets the DirectoryInfo object at the given Index.</summary>
		public DirectoryInfo this[int Index]
		{
			get { return (DirectoryInfo)this.InnerList[Index]; }
			set { this.InnerList[Index] = value; }
		}

        private long dirSize;

        public long DirSize
        {
            get { return dirSize; }
            set { dirSize = value; }
        }        

		#endregion

		#region Add/Remove Functions

		/// <summary>Adds a new DirectoryInfo object into the end of the collection.</summary>
		/// <param name="Item">The DirectoryInfo object to add.</param>
		public void Add(DirectoryInfo Item)
		{
			this.InnerList.Add(Item);
		}

		/// <summary>Adds a collection of DirectoryInfo objects into the end of the collection.</summary>
		/// <param name="Items">The array of DirectoryInfo objects to add.</param>
		public void AddRange(DirectoryInfo[] Items)
		{
			this.InnerList.AddRange(Items);
		}

		/// <summary>Adds a collection of DirectoryInfo objects into the end of the collection.</summary>
		/// <param name="Items">The DirectoryInfoCollection containing the DirectoryInfo objects to add.</param>
		public void AddRange(DirectoryInfoCollection Items)
		{
			this.InnerList.AddRange(Items);
		}

		/// <summary>Removes the specified DirectoryInfo from the collection.</summary>
		/// <param name="Item">The DirectoryInfo object to remove.</param>
		public void Remove(DirectoryInfo Item)
		{
			this.InnerList.Remove(Item);
		}

		#endregion

		#region Sorting Functions

		/// <summary>Sorts the DirectoryInfo objects in the collection using the specified comparer.</summary>
		/// <param name="Comparer">The comparer to use to determine the sorting of the objects.</param>
		public void Sort(IComparer Comparer)
		{
			this.InnerList.Sort(Comparer);
		}

		/// <summary>Sorts the DirectoryInfo objects in the collection by Name.</summary>
		public void SortByName()
		{
			this.InnerList.Sort(new DirectoryInfoNameComparer());
		}

		/// <summary>Sorts the DirectoryInfo objects in the collection by LastWriteTime.</summary>
		public void SortByLastWriteTime()
		{
			this.InnerList.Sort(new DirectoryInfoLastWriteTimeComparer());
		}

		/// <summary>Reverses the order of the DirectoryInfo objects in the collection.</summary>
		public void Reverse()
		{
			this.InnerList.Reverse();
		}

		#endregion

		#region Searching Functions

		/// <summary>Does the specified directory exist within the collection?</summary>
		/// <param name="FilePath">The directory to look for.</param>
		/// <returns>Whether the directory exists within the collection.</returns>
		public bool Contains(string DirectoryPath)
		{
			foreach (DirectoryInfo objFile in this)
			{
				if (objFile.FullName == DirectoryPath)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Does the specified DirectoryInfo object exist within the collection?</summary>
		/// <param name="Directory">The DirectoryInfo object to look for.</param>
		/// <returns>Whether the DirectoryInfo object exists within the collection.</returns>
		public bool Contains(DirectoryInfo Directory)
		{
			return this.InnerList.Contains(Directory);
		}

		/// <summary>Searches within the collection for the specified DirectoryInfo object.</summary>
		/// <param name="Directory">The DirectoryInfo object to search for.</param>
		/// <returns>The zero-based index of the DirectoryInfo object object within the collection.</returns>
		public int IndexOf(DirectoryInfo Directory)
		{
			return this.InnerList.IndexOf(Directory);
		}

		/// <summary>
		///		Searches within the collection for any DirectoryInfo objects who's name
		///		matches the supplied expression.
		/// </summary>
		/// <param name="Match">The Regular Expression to search with.</param>
		/// <returns>A collection of matching DirectoryInfo objects.</returns>
		public DirectoryInfoCollection FilterByName(Regex Match)
		{
			DirectoryInfoCollection objReturn = new DirectoryInfoCollection();
			foreach (DirectoryInfo objItem in this)
			{
				if (Match.IsMatch(objItem.Name))
				{
					objReturn.Add(objItem);
				}
			}
			return objReturn;
		}

		#endregion

		#region Comparer Internal Classes

		internal class DirectoryInfoNameComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				DirectoryInfo objX = (DirectoryInfo)x;
				DirectoryInfo objY = (DirectoryInfo)y;
				return objX.Name.CompareTo(objY.Name);
			}
		}
		
		internal class DirectoryInfoLastWriteTimeComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				DirectoryInfo objX = (DirectoryInfo)x;
				DirectoryInfo objY = (DirectoryInfo)y;
				return objX.LastWriteTime.CompareTo(objY.LastWriteTime);
			}
		}

		#endregion

	}
}