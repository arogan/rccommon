/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.IO;
using System.Management;
using System.Diagnostics;
using System.Configuration;
using System.Net;
using System.Runtime.InteropServices;
//using System.Reflection.Assembly;
//using System.Diagnostics.FileVersionInfo;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Summary description for rcShared.
	/// </summary>
	public class RcShared
	{
        public const string CRLF = "\r\n";
        public const long GBDIVISOR = 1000000000;
        public const long MBDIVISOR = 1000000;
        public const long KBDIVISOR = 1000;
		private static System.Text.RegularExpressions.Regex _isNumber = new System.Text.RegularExpressions.Regex(@"^\d+$");


        #region elapsed time
        public enum TimeUnit
		{
			Days = 0,
			Hours = 1,
			Minutes = 2,
			Seconds = 3
		}

		public class ElapsedTime
		{
			public System.DateTime StartDt = System.DateTime.Now;
			public System.DateTime StopDt;
			public System.TimeSpan ElapseTS;

			public ElapsedTime()
			{
			}

			public double SecondsDbl()
			{
				ElapseTS = StopDt.Subtract(StartDt);
				return ElapseTS.TotalSeconds;
			}

			public string Seconds()
			{
				ElapseTS = StopDt.Subtract(StartDt);
				return ElapseTS.TotalSeconds.ToString("0.000 sec");
			}

			public String Minutes()					
			{
				ElapseTS = StopDt.Subtract(StartDt);
				return ElapseTS.TotalMinutes.ToString("0.000 min");
			}

			public String Hours()					
			{
				ElapseTS = StopDt.Subtract(StartDt);
				return ElapseTS.TotalHours.ToString("0.000 hrs");
			}

			public String Days()					
			{
				ElapseTS = StopDt.Subtract(StartDt);
				return ElapseTS.TotalDays.ToString("0.000 days");
			}

			public void SetStart()
			{
				StartDt = System.DateTime.Now;
			}

			public void SetStop()
			{
				StopDt = System.DateTime.Now;
			}

            /// <summary>
            /// days hrs min seconds
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                string ret = "";
                ElapseTS = StopDt.Subtract(StartDt);
                if (ElapseTS.TotalHours > 24)
                    ret = ret + ElapseTS.Days.ToString("0 days ");
                if (ElapseTS.TotalMinutes > 60)
                    ret = ret + ElapseTS.Hours.ToString("0 hrs ");
                if (ElapseTS.TotalSeconds > 60)
                    ret = ret + ElapseTS.Minutes.ToString("0 min ");
                
                ret = ret + ElapseTS.Seconds.ToString("0 sec");
                return ret;
            }
        }
        #endregion

        public RcShared()
		{
			//
			// TODO: Add constructor logic here
			//
        }

        #region Logging
        public static void OpenLog()
		{
			string fname = GetLogName();
			OpenLog(fname);
		}
	
		public static void OpenLog(string afile)
		{			
			System.Diagnostics.Process proc = new System.Diagnostics.Process();
			proc.EnableRaisingEvents=false;
			proc.StartInfo.FileName="notepad";
			proc.StartInfo.Arguments=afile;
			proc.Start();
		}
		public static string ReadLog()
		{
			string fname = GetLogName();
			return ReadLog(fname);
		}
		public static string ReadLog(string fileName)
		{			
			FileInfo f = new FileInfo(fileName);
			if (f.Exists)
			{
				StreamReader sr = new StreamReader(f.FullName);				
				string str =  sr.ReadToEnd();
				sr.Close();
				return str;
			}
			return "";			
		}

		public static string ReadLog(string fileName, int lenK)
		{			
			FileInfo f = new FileInfo(fileName);
			int len = lenK * 1000;
			if (f.Exists)
			{
				StreamReader sr = new StreamReader(f.FullName);				
				string str =  sr.ReadToEnd();
				if (len < str.Length)
					str = str.Substring(str.Length - len, len);
				sr.Close();
				return str;
			}
			return "";			
		}

		public static void TrimLog()
		{
			string fname = GetLogName();
			TrimLog(fname);
		}

		public static void TrimLog(string fileName)
		{
			TrimLog(fileName,1000,1500);
		}

		public static void TrimLog(int minSize, int maxSize)
		{
			string fname = GetLogName();
			TrimLog(fname,minSize,maxSize);
		}
	
		public static void TrimLog(string fileName, int minSize, int maxSize)
		{
			try
			{
				minSize = minSize * 1000;
				maxSize = maxSize * 1000;
				FileInfo f = new FileInfo(fileName);
				if (f.Exists)
				{
					if (f.Length >= maxSize)
					{
						StreamReader sr = new StreamReader(f.FullName);
						//char[] buffer = new char[minSize];
						//sr.ReadBlock(buffer,Convert.ToInt32(f.Length - minSize),minSize-1);
						//sr.Read(buffer,Convert.ToInt32(f.Length - minSize),minSize-1);
						string str = sr.ReadToEnd();
						str = str.Substring(str.Length - minSize,minSize);
						sr.Close();
						f.Delete();
						StreamWriter sw = f.CreateText();
						sw.Write(str);
						sw.Flush();
						sw.Close();
						WriteLog(fileName,"Log was trimmed to " + minSize.ToString());
					}
				}
			}
			catch (Exception ex)
			{
				//do nothing
			}
		}

		/// <summary>
		/// write a line of text to a file
		/// </summary>
		/// <param name="fileName">full filename including path</param>
		/// <param name="msg">message to write to above file</param>
		public static void WriteLog(string fileName, string msg)
		{
			try
			{
				FileInfo myFile;
				myFile = new FileInfo(fileName);
				StreamWriter sw;

				if (File.Exists(fileName))
				{
					sw = myFile.AppendText();
				}
				else
				{
					sw = myFile.CreateText();
				}
				sw.WriteLine(msg);
				sw.Flush();
				sw.Close();
			}
			catch (Exception ex)
			{
				//do nothing
			}
		}		

		public static string AddServerName(string msg)
		{
			return "[" + RcShared.GetMachineName() + "] " + msg;
		}

		public static void WriteLogDt(string fileName, string msg)
		{
			WriteLog(fileName, DateTime.Now + "  " + msg);
		}

		public static void WriteLog(string msg)
		{
			string fname = GetLogName();
			msg = DateTime.Now + "  " + msg;
			WriteLog(fname,msg);
		}	
		
        /// <summary>
        /// modified to include complete path now. covers case where app is launched from a windows service and the log ends up in the system32 directory.
        /// </summary>
        /// <returns></returns>
		public static string GetLogName()
		{
			//string fname = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".log";
            string fname = System.Reflection.Assembly.GetEntryAssembly().GetName().Name + ".log";
            fname = GetExePath(false) + @"\" + fname;
			return fname;
        }

        public static string GetExeBaseName()
        {
            string fname = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            fname = GetExePath(false) + @"\" + fname;
            return fname;
        }
        #endregion


        #region Shell Process
        public static int RunShellSync(string cmd, string arg)
        {
            Process p = new Process();
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.EnableRaisingEvents = false;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                p.StartInfo.CreateNoWindow = false;
                p.StartInfo.UseShellExecute = true;
                p.Start();
                p.WaitForExit();
                if (p.HasExited)
                {
                    p.Close();
                    //return p.ExitCode;
                    return 0;
                }
                else
                {
                    p.Close();
                    //still running
                    return -999;
                }
            }
            catch (System.Exception ex)
            {
                WriteEx("RunShellSync", ex);
                throw ex;
            }
        }

        public static int RunShellSyncMax(string cmd, string arg)
        {
            Process p = new Process();
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.EnableRaisingEvents = false;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                p.StartInfo.CreateNoWindow = false;
                p.StartInfo.UseShellExecute = true;
                p.Start();
                p.WaitForExit();
                if (p.HasExited)
                {
                    p.Close();
                    //return p.ExitCode;
                    return 0;
                }
                else
                {
                    p.Close();
                    //still running
                    return -999;
                }
            }
            catch (System.Exception ex)
            {
                WriteEx("RunShellSync", ex);
                throw ex;
            }
        }

        public static int RunShellSyncHide(string cmd, string arg)
        {
            Process p = new Process();
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.EnableRaisingEvents = false;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = true;
                p.Start();
                p.WaitForExit();
                if (p.HasExited)
                {
                    p.Close();
                    //return p.ExitCode;
                    return 0;
                }
                else
                {
                    p.Close();
                    //still running
                    return -999;
                }
            }
            catch (System.Exception ex)
            {
                WriteEx("RunShellSyncHide", ex);
                throw ex;
            }
        }

        public static int RunShellSync(string cmd, string arg, ref System.DateTime startDt,
                                       ref System.DateTime endDt, ref double elapse, TimeUnit timeUnit)
        {
            try
            {
                int ret = -999;
                //cmd = cmd + " " + arg;
                Process p = new Process();
                p.StartInfo.FileName = cmd;
                p.StartInfo.UseShellExecute = true;
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(cmd);
                p.StartInfo.Arguments = arg;
                //Process p = System.Diagnostics.Process.Start(cmd,"");
                p.Start();
                startDt = p.StartTime;
                p.WaitForExit();
                if (p.HasExited)
                {
                    ret = p.ExitCode;
                }
                endDt = p.ExitTime;
                elapse = DateDiff(startDt, endDt, timeUnit);
                p.Close();
                return ret;
            }
            catch (System.Exception ex)
            {
                WriteEx("RunShellSync", ex);
                throw ex;
            }
        }

        public static int RunSync(string cmd, string arg, ref System.DateTime startDt,
           ref System.DateTime endDt, ref double elapse, TimeUnit timeUnit, ref string outStr,
           ref string errStr)
        {
            return RunSync(cmd, arg, ref startDt, ref endDt, ref elapse, timeUnit, ref outStr, ref errStr, ProcessWindowStyle.Minimized, ProcessPriorityClass.Normal);
        }


        public static int RunSync(string cmd, string arg, ref System.DateTime startDt,
            ref System.DateTime endDt, ref double elapse, TimeUnit timeUnit, ref string outStr,
            ref string errStr, ProcessWindowStyle winStyle, ProcessPriorityClass priority)
        {
            try
            {
                int ret = -999;

                Process p = new Process();
                p.StartInfo.FileName = cmd;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(cmd);
                p.StartInfo.Arguments = arg;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.WindowStyle = winStyle;

                p.Start();

                if (!p.HasExited)
                {
                    p.PriorityClass = priority;
                }

                startDt = p.StartTime;

                StreamReader sr = p.StandardOutput;
                StreamReader sre = p.StandardError;
                StringBuilder sb = new StringBuilder();
                StringBuilder sbe = new StringBuilder();

                while (!p.HasExited)
                {
                    RcSharedEx.DoEvents();
                    while (!p.HasExited && !sr.EndOfStream)
                    {
                        sb.Append(sr.ReadToEnd());
                        RcSharedEx.DoEvents();                       
                    }
                    while (!p.HasExited && !sre.EndOfStream)
                    {
                        sbe.Append(sre.ReadToEnd());
                        RcSharedEx.DoEvents();                        
                    }
                    RcShared.Sleep(1);       
                }

                if (!sr.EndOfStream)
                    sb.Append(sr.ReadToEnd());
                if (!sre.EndOfStream)
                    sbe.Append(sre.ReadToEnd());
                                                
                if (p.HasExited)
                {
                    sr.Close();
                    sre.Close();
                    ret = p.ExitCode;
                    outStr = sb.ToString();
                    errStr = sbe.ToString();
                }
                endDt = p.ExitTime;
                elapse = DateDiff(startDt, endDt, timeUnit);
                p.Close();
                return ret;
            }
            catch (System.Exception ex)
            {
                WriteEx("RunSync", ex);
                throw ex;
            }
        }

        public static int RunSync(string cmd, string arg, ref string outStr, ref string errStr)
        {
            return RunSync(cmd, arg, ref outStr, ref errStr, ProcessWindowStyle.Minimized, ProcessPriorityClass.Normal);
        }

        public static int RunSync(string cmd, string arg, ref string outStr, ref string errStr, ProcessWindowStyle winStyle, ProcessPriorityClass priority)
        {
            DateTime startdt = System.DateTime.Now;
            DateTime stopdt = System.DateTime.Now;
            double elapse = 0d;
            return RunSync(cmd, arg, ref startdt, ref stopdt, ref elapse, TimeUnit.Seconds, ref outStr, ref errStr, winStyle, priority);
        }

        /// <summary>
        /// add the following to your class:
        ///	private System.Diagnostics.Process process1 = new System.Diagnostics.Process();
        ///	
        ///	this.process1.SynchronizingObject = this;
        /// this.process1.Exited += new System.EventHandler(this.process1_Exited);
        /// 
        /// private void process1_Exited(object sender, System.EventArgs e)
        /// 
        /// example call:
        /// RcShared.RunShellAsync(process1,"notepad","");
        /// </summary>
        /// <param name="p"></param>
        /// <param name="cmd"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static System.DateTime RunShellAsync(Process p, string cmd, string arg, ProcessWindowStyle winStyle)
        {
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.EnableRaisingEvents = true;
                //p.PriorityClass = ProcessPriorityClass.AboveNormal;                               
                p.StartInfo.WindowStyle = winStyle;
                p.Start();


                return p.StartTime;
            }
            catch (System.Exception ex)
            {
                WriteEx("RunShellAsync", ex);
                throw ex;
            }
        }

        public static System.DateTime RunShellAsync(Process p, string cmd, string arg)
        {
            return RunShellAsync(p, cmd, arg, ProcessWindowStyle.Normal);
        }

        public static void RunShellAsyncNoRet(Process p, string cmd, string arg, ProcessWindowStyle winStyle)
        {
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.EnableRaisingEvents = true;
                //p.PriorityClass = ProcessPriorityClass.AboveNormal;
                p.StartInfo.WindowStyle = winStyle;
                p.Start();

            }
            catch (System.Exception ex)
            {
                WriteEx("RunShellAsync", ex);
                throw ex;
            }
        }


        public static void RunShellAsyncNoRet(Process p, string cmd, string arg)
        {
            RunShellAsyncNoRet(p, cmd, arg, ProcessWindowStyle.Normal);
        }

        public static System.DateTime RunShellAsyncHide(Process p, string cmd, string arg)
        {
            return RunShellAsync(p, cmd, arg, ProcessWindowStyle.Hidden);
        }

        public static void RunAsync(Process p, string cmd, string arg)
        {
            try
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = arg;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;

                p.EnableRaisingEvents = true;
                p.Start();
            }
            catch (System.Exception ex)
            {
                WriteEx("RunAsync", ex);
                throw ex;
            }
        }

        public static int RunSyncForceNoWindow(string cmd, string arg, ref string outStr,
            ref string errStr, ProcessPriorityClass priority)
        {
            Process p = new Process();
            try
            {
                int ret = -999;

                p.StartInfo.FileName = cmd;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(cmd);
                p.StartInfo.Arguments = arg;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = false;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.CreateNoWindow = true;

                p.Start();

                if (!p.HasExited && priority != ProcessPriorityClass.Normal)
                {
                    p.PriorityClass = priority;
                }

                StreamReader sr = p.StandardOutput;
                StringBuilder sb = new StringBuilder();
                //long c = 0;
                while (!p.HasExited)
                {
                    RcSharedEx.DoEvents();
                    while (!p.HasExited && !sr.EndOfStream)
                    {
                        sb.Append(sr.ReadToEnd());
                        RcSharedEx.DoEvents();
                        RcShared.Sleep(1);
                        //c++;
                    }
                }
                //RcShared.WriteLog("stream count = " + c.ToString("N0"));
                if (!sr.EndOfStream)
                    sb.Append(sr.ReadToEnd());

                //p.WaitForExit();
                if (p.HasExited)
                {
                    sr.Close();
                    ret = p.ExitCode;
                    outStr = sb.ToString();
                    //outStr = p.StandardOutput.ReadToEnd();
                    //errStr = p.StandardError.ReadToEnd();
                    errStr = "";
                }                
                return ret;
            }
            catch (System.Threading.ThreadAbortException ta)
            {
                RcShared.WriteWarn("RunSyncForceNoWindow: " + ta.Message);
                return -999;
            }
            catch (System.Exception ex)
            {
                RcShared.WriteEx("RunSyncForceNoWindow", ex);
                throw ex;
            }
            finally
            {
                if (!p.HasExited)
                {
                    p.Kill();
                }
                p.Close();
            }
        }
        #endregion


        /// <summary>
		/// get full path and name of exe running minus the extension
		/// </summary>
		/// <returns></returns>
//		public static string GetBaseName()
//		{
//			//string fpath = System.Reflection.Assembly.GetExecutingAssembly().GetName().Location;
//			//return Path.GetFileNameWithoutExtension(fpath);
//			return GetExePath(false) + @"\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
//		}

		public static string GetExePath(bool unc)
		{
            string fpath = AppDomain.CurrentDomain.BaseDirectory;
            fpath = Path.GetDirectoryName(fpath).ToString();

            //string fpath = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;            
            //fpath = Path.GetDirectoryName(fpath);
            //fpath = fpath.Replace(@"file:\","");

			if (!unc) return fpath;
			int i = fpath.LastIndexOf(":");
			if (i > 0)
			{
				fpath = fpath.Substring(i + 1);
			}
			fpath = @"\\" + RcShared.GetMachineName() + fpath;
			return fpath;
		}

		public static void OWriteLog(string fileName, string msg)
		{
			if(File.Exists(fileName))
			{
				File.Delete(fileName);
			}
			WriteLog(fileName,msg);
		}

		public static void OWriteLogDt(string fileName, string msg)
		{
			if(File.Exists(fileName))
			{
				File.Delete(fileName);
			}
			WriteLogDt(fileName,msg);
		}

		public static void WriteError(string msg)
		{
			WriteLog("ERROR: " + msg);
		}

		public static void WriteError(string fileName, string msg)
		{
			WriteLogDt(fileName, "ERROR: " + msg);
		}

        public static void WriteEx(System.Exception ex)
        {
            WriteError(ex.Message);
        }

		public static void WriteEx(string fxName, System.Exception ex)
		{
			WriteError(fxName + ": " + ex.Message);
		}

		public static void WriteEx(string fileName, string fxName, System.Exception ex)
		{
			WriteError(fileName, fxName + ": " + ex.Message);
		}

		public static void WriteWarn(string msg)
		{
			WriteLog("WARNING: " + msg);
		}

		public static void WriteWarn(string fileName, string msg)
		{
			WriteLogDt(fileName, "WARNING: " + msg);
		}

		public static string ReadLine(string fileName)
		{			
			StreamReader sr;
			string result = "";

			if (File.Exists(fileName))
			{
				sr = new StreamReader(fileName);
				result = sr.ReadLine();
				if (result == null) result = "";
				sr.Close();			
			}
			return result;
		}

		public static long GetUsedSpace(string dir)
		{
			DirectoryInfo d = new DirectoryInfo(dir);
			long size = 0;

			if (d.Exists)
			{
				DirectoryInfo[] dirs;
				dirs = d.GetDirectories();
				//got subs, loop through subdirs
				foreach (DirectoryInfo d2 in dirs)
				{
					//recurse
					size += GetUsedSpace(d2.FullName);
				}
				
				//no more subdirectories
				foreach (FileInfo f in d.GetFiles())
				{
					size += f.Length;
				}				
			}
			return size;
		}

		public static long GetFreeSpace(string driveLetter)
		{									
			ManagementObject disk = new	 ManagementObject("win32_logicaldisk.deviceid=\"" + driveLetter.ToUpper() + ":\"");
			disk.Get();
			return Convert.ToInt64(disk["FreeSpace"]);
		}

//		public static long GetFreeSpaceUNC(string unc)
//		{									
//			string d = getDrvLtrFromUNC(unc);
//			return GetFreeSpace(d);
//		}
//
//		public static string getDrvLtrFromUNC(string UNCPath)
//		{ // get all the named drives then see whose ProviderName
//			// matches the UNCPath, then return that drive letter ( form of "e: " - 2 chars)
//			// empty string would be an unmapped or invalid drive path.
//			string retVal="";
//			string thisDescrip="";
//			string thisUNCroot="";
//			string ltrDr="";
//			string wackyQuery="";
//			string [] logDrives=Directory.GetLogicalDrives();
//            
//			for (int i = 0; i<logDrives.GetLength(0);i++)
//			{
//				ltrDr=logDrives[i].Substring(0,2);
//				wackyQuery =string.Format("Win32_LogicalDisk.DeviceID=\"{0}\"",ltrDr);
//				ManagementObject moDrive =new ManagementObject(wackyQuery);
//				thisDescrip= moDrive.Properties["Description"].Value.ToString();
//				if (thisDescrip.IndexOf("Network")>-1)
//				{
//					thisUNCroot = moDrive.Properties["ProviderName"].Value.ToString();
//					if (thisUNCroot.ToLower().IndexOf(UNCPath.ToLower())>-1)
//					{
//						retVal = ltrDr;
//						break;
//					}
//				} // end of a network drive
//
//			} // end of for loop
//        
//			return retVal;
//		}

		/// <summary>
		/// Gets the disk free space on a particular volume.
		/// </summary>
		/// <param name="lpszPath">Full path of folder on required volume. Must end with a backslash!</param>
		/// <param name="lpFreeBytesAvailable">Free bytes available on the volume for the current user.</param>
		/// <param name="lpTotalNumberOfBytes">Total number of bytes available on the volume for the current user.</param>
		/// <param name="lpTotalNumberOfFreeBytes">Total number of bytes available on the volume for all users.</param>
		/// <returns>True if successful, false on error.</returns>

		[DllImport ("Kernel32")]
		public static extern bool GetDiskFreeSpaceEx
			(
			string  lpszPath,       // Must name a folder, must end with '\'.
			ref long lpFreeBytesAvailable,
			ref long lpTotalNumberOfBytes,
			ref long lpTotalNumberOfFreeBytes
			);


		/// <summary>
		/// Returns free space for drive containing the specified folder, or returns -1 on failure.
		/// </summary>
		/// <param name="folder_name">Must name a folder, and MUST end with a backslash.</param>
		/// <returns>Space free on the volume containing 'folder_name' or -1 on error.</returns>

		public static long GetFreeSpaceUNC( string folder_name )
		{
			long free=0, dummy1=0, dummy2=0 ;

			if ( GetDiskFreeSpaceEx( folder_name, ref free, ref dummy1,
				ref dummy2 ) )
				return free ;
			else
				return -1 ;
		}


//		public static long GetUsedSpace(DirectoryInfo  dp)
//		{
//			long results = 0;
//			foreach(DirectoryInfo d in d.GetDirectories())
//			{
//				results = results + GetUsedSpace(d);
//			}
//			foreach(FileInfo f in dp.GetFiles())
//			{
//				results = results + f.Length;
//			}
//			return results;
//		}

		/// <summary>
		/// Searches a 2 dimentional string array.  used to look up name value pairs
		/// </summary>
		/// <param name="name"></param>
		/// <param name="ary"></param>
		/// <returns></returns>
		public static string SearchArray(string name, string[,] ary)
		{
			string s = "";
			for (int i = 0; i < ary.Length / 2; i++)
			{
				s = ary[i,0];
				if (name == s)
				{
					return ary[i,1];
				}
			}
			return "";
		}

		/// <summary>
		/// given a path it will return the drive letter.  If the string is invalid then "" is returned.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static string GetRootDrive(string path)
		{
			string ret = "";
			if (path.Length > 0)
			{
				if (path.Substring(1,1) == ":")
				{
					ret = path.Substring(0,1);
				}
			}
			return ret;
		}

        public static long GetDirSize(string rootdir)
        {       
            long DirSize = 0;
            //DirectoryInfo[] DI = new DirectoryInfo(rootdir).GetDirectories("*.*", SearchOption.AllDirectories);
            FileInfo[] FI = new DirectoryInfo(rootdir).GetFiles("*.*", SearchOption.AllDirectories);
            foreach (FileInfo F1 in FI)
            {
                DirSize += F1.Length;
            }
            return DirSize;                       
        }

		public static bool PrevInstance()
		{
			Process currentProcess = Process.GetCurrentProcess();
			Console.WriteLine("Process name=" + currentProcess.ProcessName);
			Process[] localByName = Process.GetProcessesByName(currentProcess.ProcessName);
			Console.WriteLine("Length=" + localByName.Length);

			if (localByName.Length > 1)
				return true;
			else
				return false;
		}
		
		public static bool InIDE()
		{
			return System.Diagnostics.Debugger.IsAttached;
		}		

		public static string GetConfig(string key)
		{
            if (Object.Equals(System.Configuration.ConfigurationManager.AppSettings[key], null))
			{
				return "";
			}
			else
			{
                return System.Configuration.ConfigurationManager.AppSettings[key];
			}
		}

        public static void WriteConfig(string key, string value)
        {            
            //ConfigurationSettings.AppSettings.Set(key, value);            
            System.Configuration.Configuration config =
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (!Object.Equals(ConfigurationSettings.AppSettings[key], null))
            {
                config.AppSettings.Settings.Remove(key);
            }
            config.AppSettings.Settings.Add(key, value);
            config.Save(ConfigurationSaveMode.Modified);                        
        }

		public static double DateDiff(System.DateTime startDt, System.DateTime endDt, TimeUnit tu)
		{			
			System.TimeSpan ts = endDt.Subtract(startDt);
			switch (tu)
			{
				case TimeUnit.Days:
					return ts.TotalDays;
					break;
				case TimeUnit.Hours:
					return ts.TotalHours;
					break;
				case TimeUnit.Minutes:
					return ts.TotalMinutes;
					break;
				case TimeUnit.Seconds:
					return ts.TotalSeconds;
					break;
			}	
			return -999;
		}

		public static int GetNextKey()
		{
			System.Random r = new Random();
			return r.Next();
		}

		public static string GetNextKeyStr()
		{
			return Convert.ToString(GetNextKey());
		}

        public static string GetGuid()
        {
            System.Guid guid = System.Guid.NewGuid();
            return guid.ToString();
        }

		public static string GetMachineName()
		{
			return Dns.GetHostName();
		}

        public static string GetIPAddress()
        {
            string strHostName = GetMachineName();
            
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            foreach (IPAddress ipa in addr)
            {
                if (!ipa.ToString().Contains(@":"))
                {
                    return ipa.ToString();
                }
            }
            return "";
        }

		public static void Sleep(int sec)
		{
			System.Threading.Thread.Sleep(sec * 1000);
		}
		public static void SleepMS(int msec)
		{
			System.Threading.Thread.Sleep(msec);
		}

		public static string Join(string[] ary)
		{
			return Join(ary,",");
		}
		public static string Join(string[] ary, string delim)
		{
			string ret = "";
			if (!object.Equals(ary,null) && ary.Length > 0)
			{
				foreach (string s in ary)
				{
					ret = ret + s + delim;
				}
				ret = ret.Substring(0,ret.Length - delim.Length);
			}
			return ret;
		}

		/// <summary>
		/// determine if it is a dts day
		///	return "spring" or "fall"
		/// </summary>
		/// <returns></returns>
		public static string CheckDSTOld()
		{
			/*
			 'determine if it is a dts day
			'return "spring" or "fall"

			'On my
			'machine, daylight savings starts on the first
			'Sunday of April, and ends on the last Sunday
			'of October, both times at 02:00 AM.
			'
			'B) To find the 1st Sunday in April, find the day of the week of April 1st;
			'let it equal X, where 0=Sunday, 1=Monday, etc.
			'if (X) X = 8 - X;
			'now X is the date of the 1st Sunday in April.
			'
			'C) To find the last Sunday in October, find the day of the week of
			'October 31st; let it equal Y, where 0=Sunday, etc.
			'Y = 31 - Y;
			'now Y is the date of the last Sunday in October.
			'
			'A couple of facts:
			'
			'US Daylight Savings time begins, first Sunday in April (but not in
			'Arizona, Hawaii, and parts of southern Indiana).
			'
			'US Daylight Savings Time ends, last Sunday in October (but not in
			'Arizona, Hawaii, and parts of southern Indiana).
			*/

			//string ret = "";
			System.DateTime spring;
			System.DateTime fall;
			int dow;
			System.DateTime dtnow = System.DateTime.Now.Date;
			int day;
			//System.DateTime dt;

			spring = new System.DateTime(dtnow.Year,4,1);
			fall = new System.DateTime(dtnow.Year,10,31);

			//check spring
			dow = (int)spring.DayOfWeek;
			day = 8 - dow;
			spring = new System.DateTime(dtnow.Year,4,day);
			//if (dtnow.Date == dt.Date)
			//	return "spring";
			
			//check fall
			dow = (int)fall.DayOfWeek;
			day = 31 - dow;
			fall = new System.DateTime(dtnow.Year,10,day);
			//if (dtnow.Date == dt.Date)
			//	return "fall";

			if (dtnow.Date >= spring.Date & dtnow.Date < fall.Date)
				return "spring";
			else
				return "fall";				
		}

        /// <summary>
		/// determine if it is a dts day
		///	return "spring" or "fall"
		/// </summary>
		/// <returns></returns>
        public static string CheckDST(int offset)
        {
            TimeZone tz = TimeZone.CurrentTimeZone;

            DateTime date = System.DateTime.Now.AddDays(offset);
            if (tz.IsDaylightSavingTime(date))
                return "spring";
            else
                return "fall";		          
        }
        public static string CheckDST()
        {
            return CheckDST(0);
        }

		public static bool IsInteger(string theValue)
		{
			System.Text.RegularExpressions.Match m = _isNumber.Match(theValue);
			return m.Success;
		} //IsInteger		

		public static void ListProcesses()
		{
			foreach (Process thisproc in Process.GetProcesses()) 
			{
				Console.WriteLine(thisproc.ProcessName);
			}		
		}

		public static void LogProcesses()
		{
			foreach (Process thisproc in Process.GetProcesses()) 
			{
				RcShared.WriteLog(thisproc.ProcessName);
			}		
		}

        public static string KillProcess(string processName)
        {
            return KillProcess(processName, false);
        }
      
		public static string KillProcess(string processName, bool forceKill)
		{
			//System.Diagnostics.Process myproc= new System.Diagnostics.Process();
			//Get all instances of proc that are open, attempt to close them.

			
			foreach (Process thisproc in Process.GetProcessesByName(processName)) 
			{
				string pn = thisproc.ProcessName;
                if (forceKill)
                {
                    thisproc.Kill();
                    return "kill";
                }
                else
                {
                    if (!thisproc.CloseMainWindow())
                    {
                        //If closing is not successful or no desktop window handle, then force termination.					                    
                        thisproc.Kill();
                        return "kill";
                    }
                    else
                    {
                        return "close";
                    }
                }
			} // next proc	
			return "";
		}

        public static int GetProcessID(string processName)
        {
            foreach (Process thisproc in Process.GetProcessesByName(processName))
            {
                string pn = thisproc.ProcessName;              
                return thisproc.Id;                         
            } // next proc	
            return -999;
        }

        public static int ByteIndexOf(byte[] ByteArrayToSearch, byte[] ByteArrayToFind)
        {
            // Any encoding will do, as long as all bytes represent a unique character.
            Encoding encoding = Encoding.GetEncoding(1252);

            string toSearch = encoding.GetString(ByteArrayToSearch, 0, ByteArrayToSearch.Length);
            string toFind = encoding.GetString(ByteArrayToFind, 0, ByteArrayToFind.Length);
            int result = toSearch.IndexOf(toFind, StringComparison.Ordinal);
            return result;
        }

        public static string DecimalToHex(int i)
        {
            return Convert.ToString(i, 16).PadLeft(2, Convert.ToChar("0")).ToUpper();
        }

        public static string DecimalToHex(byte b)
        {
            return Convert.ToString(Convert.ToInt32(b), 16).PadLeft(2, Convert.ToChar("0")).ToUpper();
        }

        public static bool IsNumeric(string s)
        {
            double dbl = 0;
            return Double.TryParse(s, out dbl);
        }

        public static string Unix2Dos(string s)
        {
            if (s.IndexOf(Environment.NewLine) > 0)
            {
                return s;
            }
            else
            {
                StringBuilder sb = new StringBuilder(s);
                sb.Replace("\n", Environment.NewLine);
                return sb.ToString();
            }
        }

        public static string GetParentDir(string aFile)
        {
            string[] ary = aFile.Split(@"\".ToCharArray());
            if (ary.Length > 1)
            {
                return ary[ary.Length - 2];
            }
            return "";
        }

        public static string StripHTML(string str)
        {
            Regex regex = new Regex("</?(.*)>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string ret = regex.Replace(str, string.Empty);
            return ret;
        }
        
        public static string ScrubFileName(string str)
        {
            if (str == null || str == String.Empty)
                return "";
            string ret = str;
            char[] chars = Path.GetInvalidFileNameChars();
            foreach (char c in chars)
            {
                ret = ret.Replace(c.ToString(),"");
            }
            return ret;
        }

        public static string RemoveText(string startStr, string EndStr, string str)
        {
            string ret = str;
            int start = str.IndexOf(startStr);
            int stop = -1;
            if (start != -1)
            {
                stop = str.IndexOf(EndStr, start);
                if (stop != -1)
                {
                    ret = str.Remove(start, stop - start + EndStr.Length);
                    ret = RemoveText(startStr, EndStr, ret);
                }
            }
            return ret;
        }

    } //end class
}
