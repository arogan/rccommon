/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.IO;
using System.Management;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Data;

namespace RcSoft.RcCommon
{
	/// <summary>
	/// Additional rcShared methods that require WinForms or VisualBasic runtime
	/// </summary>
	public class RcSharedEx : RcShared
	{
		public RcSharedEx()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void Beep()
		{
			Microsoft.VisualBasic.Interaction.Beep();
		}

        public static void Beep(int c)
        {
            for (int i=0; i < c; i++ )
            {
                Microsoft.VisualBasic.Interaction.Beep();
                Wait(1);
            }

        }


        public static void Wait(int seconds)
		{
			int finish = Environment.TickCount + seconds * 1000;
			while (Environment.TickCount < finish)
			{
				System.Windows.Forms.Application.DoEvents();                
			}			
		}

		public static void DoEvents()
		{
			System.Windows.Forms.Application.DoEvents();            
		}

		public static void SizeColumnsToContent(DataGrid dataGrid,
			int nRowsToScan)
		{
			// Create graphics object for measuring widths.
			Graphics Graphics = dataGrid.CreateGraphics();

			// Define new table style.
			DataGridTableStyle tableStyle = new DataGridTableStyle();

			try
			{
				DataTable dataTable = (DataTable)dataGrid.DataSource;

				if (-1 == nRowsToScan)
				{
					nRowsToScan = dataTable.Rows.Count;
				}
				else
				{
					// Can only scan rows if they exist.
					nRowsToScan = System.Math.Min(nRowsToScan,
						dataTable.Rows.Count);
				}

				// Clear any existing table styles.
				dataGrid.TableStyles.Clear();

				// Use mapping name that is defined in the data source.
				tableStyle.MappingName = dataTable.TableName;

				// Now create the column styles within the table style.
				DataGridTextBoxColumn columnStyle;
				int iWidth;

				for (int iCurrCol = 0;
					iCurrCol < dataTable.Columns.Count; iCurrCol++)
				{
					DataColumn dataColumn = dataTable.Columns[iCurrCol];

					columnStyle = new DataGridTextBoxColumn();

					columnStyle.TextBox.Enabled = true;
					columnStyle.HeaderText = dataColumn.ColumnName;
					columnStyle.MappingName = dataColumn.ColumnName;

					// Set width to header text width.
					iWidth = (int)(Graphics.MeasureString
						(columnStyle.HeaderText,
						dataGrid.Font).Width);

					// Change width, if data width is
					// wider than header text width.
					// Check the width of the data in the first X rows.
					DataRow dataRow;
					for (int iRow = 0; iRow < nRowsToScan; iRow++)
					{
						dataRow = dataTable.Rows[iRow];

						if (null != dataRow[dataColumn.ColumnName])
						{
							int iColWidth = (int)(Graphics.MeasureString
								(dataRow.ItemArray[iCurrCol].ToString(),
								dataGrid.Font).Width);
							iWidth = (int)System.Math.Max(iWidth, iColWidth);
						}
					}
					columnStyle.Width = iWidth + 4;

					// Add the new column style to the table style.
					tableStyle.GridColumnStyles.Add(columnStyle);
				}
				// Add the new table style to the data grid.
				dataGrid.TableStyles.Add(tableStyle);
			}
			catch(Exception e)
			{
				MessageBox.Show(e.Message);
			}
			finally
			{
				Graphics.Dispose();
			}
		}

        public static void CenterForm(System.Windows.Forms.Form frm)
        {            
            int h = Screen.PrimaryScreen.WorkingArea.Height;
            int w = Screen.PrimaryScreen.WorkingArea.Width;
            frm.Left = Convert.ToInt32(Math.Round((w - frm.Width) / 2.0, 0));
            frm.Top = Convert.ToInt32(Math.Round((h - frm.Height) / 2.0, 0));
        }

        public static Screen GetSecondaryScreen()
        {
            if (Screen.AllScreens.Length == 1)
            {
                return null;
            }

            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen.Primary == false)
                {
                    return screen;
                }
            }

            return null;
        }
	}//end class
}
