﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RcSoft.RcCommon
{
    public class DirectoryInfoEx
    {
        public DirectoryInfo DirInfo = null;
        public long Length = 0;
        public bool Processed = false;

        public DirectoryInfoEx(DirectoryInfo di)
        {
            DirInfo = di;
        }

        public long GetLength()
        {
            Length = RcShared.GetDirSize(this.DirInfo.FullName);
            return Length;
        }

        public DirectoryInfoCollectionEx GetDirectories()
        {
            DirectoryInfoCollectionEx dic = new DirectoryInfoCollectionEx();
            DirectoryInfo[] dis = this.DirInfo.GetDirectories();
            foreach (DirectoryInfo d in dis)
            {
                DirectoryInfoEx dex = new DirectoryInfoEx(d);
                dex.GetLength();
                dic.Add(dex);
            }
            return dic;
        }
    }
}
