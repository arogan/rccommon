/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using RcSoft.RcCommon;

namespace rcCommonTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{

		private RcData myData;
		private DataSet myDS;
		private DataTable myTable;
		
		private System.Windows.Forms.Button btnCreate;
		private System.Windows.Forms.DataGrid dg1;
		private System.Windows.Forms.Button btnGet;
		private System.Windows.Forms.Button btnUpdate;
		private System.Windows.Forms.TextBox txtStatus;
		private System.Windows.Forms.Button btnGetSql;
		private System.Windows.Forms.Button btnExecSql;
		private System.Windows.Forms.Button btnExecSqlStatic;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCreate = new System.Windows.Forms.Button();
			this.dg1 = new System.Windows.Forms.DataGrid();
			this.btnGet = new System.Windows.Forms.Button();
			this.btnUpdate = new System.Windows.Forms.Button();
			this.txtStatus = new System.Windows.Forms.TextBox();
			this.btnGetSql = new System.Windows.Forms.Button();
			this.btnExecSql = new System.Windows.Forms.Button();
			this.btnExecSqlStatic = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
			this.SuspendLayout();
			// 
			// btnCreate
			// 
			this.btnCreate.Location = new System.Drawing.Point(16, 16);
			this.btnCreate.Name = "btnCreate";
			this.btnCreate.Size = new System.Drawing.Size(88, 23);
			this.btnCreate.TabIndex = 0;
			this.btnCreate.Text = "Create rcData";
			this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
			// 
			// dg1
			// 
			this.dg1.DataMember = "";
			this.dg1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dg1.Location = new System.Drawing.Point(8, 56);
			this.dg1.Name = "dg1";
			this.dg1.Size = new System.Drawing.Size(728, 328);
			this.dg1.TabIndex = 2;
			// 
			// btnGet
			// 
			this.btnGet.Location = new System.Drawing.Point(104, 16);
			this.btnGet.Name = "btnGet";
			this.btnGet.TabIndex = 3;
			this.btnGet.Text = "Get DS";
			this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
			// 
			// btnUpdate
			// 
			this.btnUpdate.Location = new System.Drawing.Point(176, 16);
			this.btnUpdate.Name = "btnUpdate";
			this.btnUpdate.TabIndex = 4;
			this.btnUpdate.Text = "Update";
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			// 
			// txtStatus
			// 
			this.txtStatus.Location = new System.Drawing.Point(8, 392);
			this.txtStatus.Multiline = true;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtStatus.Size = new System.Drawing.Size(728, 80);
			this.txtStatus.TabIndex = 5;
			this.txtStatus.Text = "";
			// 
			// btnGetSql
			// 
			this.btnGetSql.Location = new System.Drawing.Point(248, 16);
			this.btnGetSql.Name = "btnGetSql";
			this.btnGetSql.TabIndex = 6;
			this.btnGetSql.Text = "Get Sql";
			this.btnGetSql.Click += new System.EventHandler(this.btnGetSql_Click);
			// 
			// btnExecSql
			// 
			this.btnExecSql.Location = new System.Drawing.Point(328, 16);
			this.btnExecSql.Name = "btnExecSql";
			this.btnExecSql.TabIndex = 7;
			this.btnExecSql.Text = "ExecSql";
			this.btnExecSql.Click += new System.EventHandler(this.btnExecSql_Click);
			// 
			// btnExecSqlStatic
			// 
			this.btnExecSqlStatic.Location = new System.Drawing.Point(408, 16);
			this.btnExecSqlStatic.Name = "btnExecSqlStatic";
			this.btnExecSqlStatic.Size = new System.Drawing.Size(88, 23);
			this.btnExecSqlStatic.TabIndex = 8;
			this.btnExecSqlStatic.Text = "ExecSql Static";
			this.btnExecSqlStatic.Click += new System.EventHandler(this.btnExecSqlStatic_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 486);
			this.Controls.Add(this.btnExecSqlStatic);
			this.Controls.Add(this.btnExecSql);
			this.Controls.Add(this.btnGetSql);
			this.Controls.Add(this.txtStatus);
			this.Controls.Add(this.btnUpdate);
			this.Controls.Add(this.btnGet);
			this.Controls.Add(this.dg1);
			this.Controls.Add(this.btnCreate);
			this.Name = "Form1";
			this.Text = "rcCommonTest";
			((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void btnCreate_Click(object sender, System.EventArgs e)
		{							
			myData = new RcData(Application.ExecutablePath);
			txtStatus.Text = myData.ConnStr;
		}

		private void btnGet_Click(object sender, System.EventArgs e)
		{

			myDS =  myData.RcGetDataset("select id1, text1, num1, dt1 from rcTest","rcTest");
			myTable = myDS.Tables["rcTest"];
			dg1.DataSource = myTable.DefaultView;
			//dg1.DataMember = "rcTest";			
			//dg1.DataSource = myDS;
		}

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{				
				myData.RcUpdate();
			}
			catch (Exception ex)
			{
				//txtStatus.Text = ex.Message + "\r\n\r\nTrace:\r\n" + ex.StackTrace;
				txtStatus.Text = ex.ToString();
			}
		}

		private void btnGetSql_Click(object sender, System.EventArgs e)
		{
			txtStatus.Text = myData.SqlText + "\r\n" + myData.RcTable;
		}

		private void btnExecSql_Click(object sender, System.EventArgs e)
		{
			try
			{
				txtStatus.Text = myData.RcExecSql("insert into rctest(text1,num1) values (\"arf\",42)").ToString();
			}
			catch (Exception ex)
			{
				txtStatus.Text = ex.ToString();
			}

		}

		private void btnExecSqlStatic_Click(object sender, System.EventArgs e)
		{
			try
			{				
				txtStatus.Text = RcData.RcExecSql("select count(*) from rctest",Application.ExecutablePath).ToString();
			}
			catch (Exception ex)
			{
				txtStatus.Text = ex.ToString();
			}
		}		
	}
}
